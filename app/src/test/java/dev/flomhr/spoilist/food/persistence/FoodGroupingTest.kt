/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.food.persistence

import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDate

class FoodGroupingTest {

    @Test
    fun `NoneGrouping should return simple list map`() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 25), category = "Produce")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 2, 10), category = "Dairy")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 3, 2), category = "Dairy")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 4, 2), category = "Produce")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val items = listOf(
            apples,
            butter,
            milk,
            potatoes,
            eggs
        )
        val expectedGroup = mapOf("" to items)

        val grouped = NoneGrouping.group(items)
        assertEquals(expectedGroup, grouped)
    }

    @Test
    fun `CategoryGrouping should return items mapped by category attribute`() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 25), category = "Produce")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 2, 10), category = "Dairy")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 3, 2), category = "Dairy")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 4, 2), category = "Produce")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val items = listOf(
            apples,
            butter,
            milk,
            potatoes,
            eggs
        )
        val expectedGroup = mapOf(
            "Dairy" to listOf(butter, milk),
            "Produce" to listOf(apples, potatoes),
            "" to listOf(eggs)
        )

        val grouped = CategoryGrouping.group(items)
        assertEquals(expectedGroup, grouped)
    }

    @Test
    fun `StorageGrouping should return items mapped by storage attribute`() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 25), storage = "Fruit basket")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 2, 10), storage = "Fridge")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 3, 2), storage = "Fridge")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 4, 2), storage = "Basement")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10), storage = "Fridge")
        val items = listOf(
            apples,
            butter,
            milk,
            potatoes,
            eggs
        )
        val expectedGroup = mapOf(
            "Fridge" to listOf(butter, milk, eggs),
            "Fruit basket" to listOf(apples),
            "Basement" to listOf(potatoes)
        )

        val grouped = StorageGrouping.group(items)
        assertEquals(expectedGroup, grouped)
    }

}