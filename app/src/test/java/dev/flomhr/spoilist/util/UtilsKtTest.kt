package dev.flomhr.spoilist.util

import android.util.Log
import androidx.compose.ui.graphics.Color
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.Assert.assertEquals
import org.junit.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

internal class UtilsKtTest {

    @Test
    fun `getColorForSpoilDate() should return Color Red when spoil date is before today`() {
        val spoilDateInThePast = LocalDate.now().minusDays(1)
        val colorForSpoilDate = getColorForSpoilDate(spoilDateInThePast)
        val colorRed = Color.Red
        assertEquals(colorRed, colorForSpoilDate)
    }

    @Test
    fun `getColorForSpoilDate() should return Color Yellow when today is AFTER warning date`() {
        val spoilDateInSomeDays = LocalDate.now().plusDays(spoilSoonWarningDays.toLong())
        val colorForSpoilDate = getColorForSpoilDate(spoilDateInSomeDays)
        val colorYellow = Color.Yellow
        assertEquals(colorYellow, colorForSpoilDate)
    }

    @Test
    fun `getColorForSpoilDate() should return Color Green when today is BEFORE warning date`() {
        val spoilDateInSomeDays = LocalDate.now().plusDays(spoilSoonWarningDays.toLong() + 1)
        val colorForSpoilDate = getColorForSpoilDate(spoilDateInSomeDays)
        val colorGreen = Color.Green
        assertEquals(colorGreen, colorForSpoilDate)
    }

    @Test
    fun `getFormattedDate() should return date in SHORT format for current locale`() {
        val date = LocalDate.of(2021, 1, 1)
        val correctFormat =
            DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.getDefault())
                .format(date)
        val formattedDate = getFormattedDate(date)
        assertEquals(correctFormat, formattedDate)
    }

    @Test
    fun `getFormattedDate() should return date in given format for given locale`() {
        val france = Locale.FRANCE
        val longStyle = FormatStyle.LONG
        val date = LocalDate.of(2021, 1, 1)
        val correctFormat =
            DateTimeFormatter.ofLocalizedDate(longStyle).withLocale(france).format(date)
        val formattedDate = getFormattedDate(date, longStyle, france)
        assertEquals(correctFormat, formattedDate)
    }

    @Test
    fun `getDateFromString() should return a LocalDate for a valid date text`() {
        mockkStatic(Log::class)
        every { Log.w(any(), any<String>()) } returns 0

        val dateString = "01.01.21"
        val expectedDate = LocalDate.of(2021, 1, 1)

        val parsedDate = getDateFromString(dateString, FormatStyle.SHORT, Locale.GERMAN)

        assertEquals(expectedDate, parsedDate)
    }

    @Test
    fun `getDateFromString() should return null for an invalid date text`() {
        mockkStatic(Log::class)
        every { Log.w(any(), any<String>()) } returns 0

        val dateString = "January 1, 2021"
        val expectedDate = null

        val parsedDate = getDateFromString(dateString, FormatStyle.LONG, Locale.GERMAN)

        assertEquals(expectedDate, parsedDate)
    }
}
