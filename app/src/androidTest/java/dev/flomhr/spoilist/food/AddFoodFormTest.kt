package dev.flomhr.spoilist.food

import android.icu.text.DateFormatSymbols
import androidx.activity.ComponentActivity
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.food.persistence.FoodItem
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

class AddFoodFormTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun nameField_shouldFocus_onScreenLoaded() {
        composeTestRule.setContent {
            val (food, setFood) = remember {
                mutableStateOf(
                    FoodItem(
                        "1",
                        "Test",
                        LocalDate.now()
                    )
                )
            }
            FoodForm(
                foodItem = food,
                setFoodItem = setFood,
                categories = listOf(),
                storageLocations = listOf()
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_nameField))
            .assertIsFocused()
    }

    @Test
    fun formFields_shouldFocus_onImeAction() {
        composeTestRule.setContent {
            val (food, setFood) = remember {
                mutableStateOf(
                    FoodItem(
                        "1",
                        "Test",
                        LocalDate.now()
                    )
                )
            }
            FoodForm(
                foodItem = food,
                setFoodItem = setFood,
                categories = listOf(),
                storageLocations = listOf()
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_nameField))
            .performClick()
            .performImeAction()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.dateDialog_title))
            .assertExists()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.categorySuggestion_textField))
            .assertIsFocused()
            .performTextInput("Test")
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.categorySuggestion_textField))
            .performImeAction()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.storageSuggestion_textField))
            .assertIsFocused()
            .performTextInput("Store")
    }

    @Test
    fun foodItem_shouldBeCreated_onFormDone() {
        val today = LocalDate.now()
        val expectedMonth =
            if (today.monthValue == 3) 2 else 3
        val expectedFood = FoodItem(
            "1",
            "Flour",
            LocalDate.of(2021, expectedMonth, 5),
            true,
            "Baking",
            "Cupboard"
        )
        var actualFood = FoodItem("", "", LocalDate.now())
        composeTestRule.setContent {
            AddFoodScreen(
                onCloseScreen = { },
                onConfirmFood = { actualFood = it },
                categories = listOf("Produce", "Frozen", "Drinks", "Baking"),
                storageLocations = listOf("Fridge", "Cupboard", "Freezer")
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_nameField))
            .performTextInput(expectedFood.name)
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_nameField))
            .performImeAction()


        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.datePicker_day_description))
            .performClick()
        composeTestRule.onNodeWithText((expectedFood.spoilDate.dayOfMonth).toString())
            .performClick()

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.datePicker_month_description))
            .performClick()
        composeTestRule.onNodeWithText(DateFormatSymbols.getInstance().months[expectedFood.spoilDate.monthValue - 1])
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.categorySuggestion_textField))
            .performTextInput("Ba")
        composeTestRule.onNodeWithText("Baking")
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.storageSuggestion_textField))
            .performTextInput("Cu")
        composeTestRule.onNodeWithText("Cupboard")
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_openedCheckbox))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.addFoodScreen_doneBtn))
            .performClick()

        // ID is generated when creating food. Set it equal here as it isn't important for equality.
        actualFood = actualFood.copy(id = expectedFood.id)
        assertEquals(expectedFood, actualFood)
    }

    @Test
    fun doneButton_shouldBeDisplayed_whenFormIsValid() {
        composeTestRule.setContent {
            AddFoodScreen(
                onCloseScreen = { },
                onConfirmFood = { },
                categories = listOf(),
                storageLocations = listOf()
            )
        }

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.addFoodScreen_doneBtn))
            .assertDoesNotExist()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_nameField))
            .performTextInput("Test Food")

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.addFoodScreen_doneBtn))
            .assertExists()
    }

    @Test
    fun doneButton_shouldBeHidden_whenFormIsInvalid() {
        composeTestRule.setContent {
            AddFoodScreen(
                onCloseScreen = { },
                onConfirmFood = { },
                categories = listOf(),
                storageLocations = listOf()
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.categorySuggestion_textField))
            .performTextInput("Category")
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.categorySuggestion_textField))
            .performImeAction()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.addFoodScreen_doneBtn))
            .assertDoesNotExist()
    }

    @Test
    fun formFields_shouldNotFunction_whenDisabled() {
        composeTestRule.setContent {
            val (food, setFood) = remember {
                mutableStateOf(
                    FoodItem(
                        "1",
                        "Test",
                        LocalDate.now()
                    )
                )
            }
            FoodForm(
                foodItem = food,
                setFoodItem = setFood,
                categories = listOf(),
                storageLocations = listOf(),
                disableFields = true
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodForm_nameField))
            .performClick()
            .assertIsNotEnabled()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dateField_textField))
            .performClick()
            .assertIsNotEnabled()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.dateDialog_title))
            .assertDoesNotExist()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.categorySuggestion_textField))
            .performClick()
            .assertIsNotEnabled()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.storageSuggestion_textField))
            .performClick()
            .assertIsNotEnabled()
    }

}