package dev.flomhr.spoilist.food

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dev.flomhr.spoilist.TestingFoodItemRepository
import dev.flomhr.spoilist.food.persistence.*
import dev.flomhr.spoilist.getOrAwaitValue
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class FoodDataHandlingTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private var viewModel = FoodListViewModel(TestingFoodItemRepository())

    @Before
    fun createViewModel() {
        viewModel = FoodListViewModel(TestingFoodItemRepository())
    }

    @Test
    fun insertNewFoodViaViewModel() {
        val food = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 1))

        viewModel.addFood(food)

        val foodList = viewModel.foodItems.getOrAwaitValue()[""]!!
        assertEquals(1, foodList.size)
        assertTrue(foodList.contains(food))
    }


    @Test
    fun removeFoodViaViewModel() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 1))
        val bread = FoodItem("5678", "Bread", LocalDate.of(2021, 2, 2))

        viewModel.addFood(apples)
        viewModel.addFood(bread)
        viewModel.removeFood(apples)

        val foodList = viewModel.foodItems.getOrAwaitValue()[""]!!
        assertEquals(1, foodList.size)
        assertTrue(foodList.contains(bread))
        assertFalse(foodList.contains(apples))
    }

    @Test
    fun sortFoodsBySpoilDateAscending() {
        val donuts = FoodItem("1213", "Donuts", LocalDate.of(2021, 2, 2))
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 3, 25))
        val cornflakes = FoodItem("9101", "Cornflakes", LocalDate.of(2021, 5, 2))
        val bread = FoodItem("5678", "Bread", LocalDate.of(2021, 5, 10))
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val expectedList = listOf(donuts, apples, cornflakes, bread, eggs)

        viewModel.addFood(apples)
        viewModel.addFood(bread)
        viewModel.addFood(cornflakes)
        viewModel.addFood(donuts)
        viewModel.addFood(eggs)

        viewModel.sortByDate(FoodListSorting.Direction.ASCENDING)

        val foodItems = viewModel.foodItems.getOrAwaitValue()[""]!!

        assertNotNull(foodItems)
        assertEquals(expectedList, foodItems)
    }

    @Test
    fun sortFoodsBySpoilDateDescending() {
        val donuts = FoodItem("1213", "Donuts", LocalDate.of(2021, 2, 2))
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 3, 25))
        val cornflakes = FoodItem("9101", "Cornflakes", LocalDate.of(2021, 5, 2))
        val bread = FoodItem("5678", "Bread", LocalDate.of(2021, 5, 10))
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val expectedList = listOf(bread, eggs, cornflakes, apples, donuts)

        viewModel.addFood(apples)
        viewModel.addFood(bread)
        viewModel.addFood(cornflakes)
        viewModel.addFood(donuts)
        viewModel.addFood(eggs)

        viewModel.sortByDate(FoodListSorting.Direction.DESCENDING)

        val foodItems = viewModel.foodItems.getOrAwaitValue()[""]!!

        assertNotNull(foodItems)
        assertEquals(expectedList, foodItems)
    }

    @Test
    fun sortFoodsByNameAscending() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 3, 25))
        val bread = FoodItem("5678", "Bread", LocalDate.of(2021, 5, 10))
        val cornflakes = FoodItem("9101", "Cornflakes", LocalDate.of(2021, 5, 2))
        val donuts = FoodItem("1213", "Donuts", LocalDate.of(2021, 2, 2))
        val apples2 = FoodItem("1415", "Apples", LocalDate.of(2021, 2, 2))
        val expectedList = listOf(apples2, apples, bread, cornflakes, donuts)

        viewModel.addFood(bread)
        viewModel.addFood(apples2)
        viewModel.addFood(donuts)
        viewModel.addFood(apples)
        viewModel.addFood(cornflakes)

        viewModel.sortByName(FoodListSorting.Direction.ASCENDING)

        val foodItems = viewModel.foodItems.getOrAwaitValue()[""]!!

        assertNotNull(foodItems)
        assertEquals(expectedList, foodItems)
    }

    @Test
    fun sortFoodsByNameDescending() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 3, 25))
        val bread = FoodItem("5678", "Bread", LocalDate.of(2021, 5, 10))
        val cornflakes = FoodItem("9101", "Cornflakes", LocalDate.of(2021, 5, 2))
        val donuts = FoodItem("1213", "Donuts", LocalDate.of(2021, 2, 2))
        val apples2 = FoodItem("1415", "Apples", LocalDate.of(2021, 2, 2))
        val expectedList = listOf(donuts, cornflakes, bread, apples2, apples)

        viewModel.addFood(bread)
        viewModel.addFood(apples2)
        viewModel.addFood(donuts)
        viewModel.addFood(apples)
        viewModel.addFood(cornflakes)

        viewModel.sortByName(FoodListSorting.Direction.DESCENDING)

        val foodItems = viewModel.foodItems.getOrAwaitValue()[""]!!

        assertNotNull(foodItems)
        assertEquals(expectedList, foodItems)
    }

    @Test
    fun sortFoodsByDefaultAscending() {
        val apples = FoodItem("A", "Apples", LocalDate.of(2021, 3, 25))
        val bread = FoodItem("B", "Bread", LocalDate.of(2021, 5, 10))
        val cornflakes = FoodItem("C", "Cornflakes", LocalDate.of(2021, 5, 2))
        val donuts = FoodItem("D", "Donuts", LocalDate.of(2021, 2, 2))
        val expectedList = listOf(apples, bread, cornflakes, donuts)

        viewModel.addFood(bread)
        viewModel.addFood(donuts)
        viewModel.addFood(cornflakes)
        viewModel.addFood(apples)

        viewModel.sortByDefault(FoodListSorting.Direction.ASCENDING)

        val foodItems = viewModel.foodItems.getOrAwaitValue()[""]!!

        assertNotNull(foodItems)
        assertEquals(expectedList, foodItems)
    }

    @Test
    fun updateFoodItemViaViewModel() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 1))
        val bread = FoodItem("5678", "Bread", LocalDate.of(2021, 1, 2))
        val newApples = apples.copy(name = "New Apples", spoilDate = LocalDate.of(2021, 1, 3))

        viewModel.addFood(apples)
        viewModel.addFood(bread)
        viewModel.updateFoodItem(newApples)

        val foodList = viewModel.foodItems.getOrAwaitValue()[""]!!
        assertEquals(2, foodList.size)
        assertTrue(foodList.contains(bread))
        assertTrue(foodList.contains(newApples))
        assertFalse(foodList.contains(apples))
        assertEquals(foodList[0], bread)
        assertEquals(foodList[1], newApples)
    }

    @Test
    fun groupFoodsByNone() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 25), category = "Produce")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 2, 10), category = "Dairy")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 3, 2), category = "Dairy")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 4, 2), category = "Produce")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val expectedMap = mapOf("" to listOf(apples, butter, milk, potatoes, eggs))

        viewModel.addFood(apples)
        viewModel.addFood(butter)
        viewModel.addFood(milk)
        viewModel.addFood(potatoes)
        viewModel.addFood(eggs)

        viewModel.groupList(NoneGrouping)

        val foodList = viewModel.foodItems.getOrAwaitValue()
        assertEquals(expectedMap, foodList)
    }

    @Test
    fun groupFoodsByCategory() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 25), category = "Produce")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 2, 10), category = "Dairy")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 3, 2), category = "Dairy")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 4, 2), category = "Produce")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val expectedMap = mapOf(
            "Dairy" to listOf(butter, milk),
            "Produce" to listOf(apples, potatoes),
            "" to listOf(eggs)
        )

        viewModel.addFood(apples)
        viewModel.addFood(butter)
        viewModel.addFood(milk)
        viewModel.addFood(potatoes)
        viewModel.addFood(eggs)

        viewModel.groupList(CategoryGrouping)

        val foodList = viewModel.foodItems.getOrAwaitValue()
        assertEquals(expectedMap, foodList)
    }

    @Test
    fun groupFoodsByStorage() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 25), storage = "Fruit basket")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 2, 10), storage = "Fridge")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 3, 2), storage = "Fridge")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 4, 2), storage = "Basement")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10), storage = "Fridge")
        val expectedMap = mapOf(
            "Fridge" to listOf(butter, milk, eggs),
            "Fruit basket" to listOf(apples),
            "Basement" to listOf(potatoes)
        )

        viewModel.addFood(apples)
        viewModel.addFood(butter)
        viewModel.addFood(milk)
        viewModel.addFood(potatoes)
        viewModel.addFood(eggs)

        viewModel.groupList(StorageGrouping)

        val foodList = viewModel.foodItems.getOrAwaitValue()
        assertEquals(expectedMap, foodList)
    }

    @Test
    fun groupFoodsByCategoryAndSortByName() {
        val apples = FoodItem("1234", "Apples", LocalDate.of(2021, 4, 25), category = "Produce")
        val butter = FoodItem("5678", "Butter", LocalDate.of(2021, 10, 10), category = "Dairy")
        val milk = FoodItem("9101", "Milk", LocalDate.of(2021, 7, 2), category = "Dairy")
        val potatoes = FoodItem("1213", "Bananas", LocalDate.of(2021, 1, 2), category = "Produce")
        val eggs = FoodItem("5678", "Eggs", LocalDate.of(2021, 5, 10))
        val expectedMap = mapOf(
            "Dairy" to listOf(butter, milk),
            "Produce" to listOf(apples, potatoes),
            "" to listOf(eggs)
        )

        viewModel.addFood(butter)
        viewModel.addFood(potatoes)
        viewModel.addFood(milk)
        viewModel.addFood(apples)
        viewModel.addFood(eggs)

        viewModel.groupList(CategoryGrouping)
        viewModel.sortByName(FoodListSorting.Direction.ASCENDING)

        val foodList = viewModel.foodItems.getOrAwaitValue()
        assertEquals(expectedMap, foodList)
    }

}