package dev.flomhr.spoilist.food

import androidx.activity.ComponentActivity
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dev.flomhr.spoilist.R
import org.junit.Rule
import org.junit.Test

class SuggestionFieldTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @OptIn(ExperimentalComposeUiApi::class)
    @Test
    fun suggestionField_shouldDisplaySuggestion_whenTypingMatchingText() {
        composeTestRule.setContent {
            val (text, setText) = remember { mutableStateOf("") }
            val suggestions = listOf("Food", "Drink", "Frozen")
            SuggestionTextField(
                value = text,
                onValueChange = { setText(it) },
                suggestions = suggestions,
                label = "Suggestions"
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.suggestionField_textField))
            .performTextInput("F")

        composeTestRule.onNodeWithText("Food")
            .assertExists()
        composeTestRule.onNodeWithText("Frozen")
            .assertExists()
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Test
    fun suggestionField_shouldNotDisplaySuggestion_whenTypingNotMatchingText() {
        composeTestRule.setContent {
            val (text, setText) = remember { mutableStateOf("") }
            val suggestions = listOf("Food", "Drink")
            SuggestionTextField(
                value = text,
                onValueChange = { setText(it) },
                suggestions = suggestions,
                label = "Suggestions"
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.suggestionField_textField))
            .performTextInput("Abc")

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.suggestionField_suggestionCard))
            .assertDoesNotExist()
        composeTestRule.onNodeWithText("Food")
            .assertDoesNotExist()
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Test
    fun suggestionField_shouldNotReact_whenDisabled() {
        composeTestRule.setContent {
            val (text, setText) = remember { mutableStateOf("F") }
            val suggestions = listOf("Food", "Drink", "Frozen")
            SuggestionTextField(
                value = text,
                onValueChange = { setText(it) },
                suggestions = suggestions,
                label = "Suggestions",
                disabled = true
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.suggestionField_textField))
            .performClick()
            .assertIsNotEnabled()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.suggestionField_suggestionCard))
            .assertDoesNotExist()
        composeTestRule.onNodeWithText("Food")
            .assertDoesNotExist()
        composeTestRule.onNodeWithText("Frozen")
            .assertDoesNotExist()
    }
}