package dev.flomhr.spoilist.food

import android.icu.text.DateFormatSymbols
import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.TextField
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusOrder
import androidx.compose.ui.focus.focusTarget
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.text.input.ImeAction
import dev.flomhr.spoilist.R
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class DateFieldTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    private val dateFormatSymbols = DateFormatSymbols.getInstance(Locale.ENGLISH)

    @Test
    fun datePicker_shouldOpen_onDateButtonClick() {
        composeTestRule.setContent {
            val (date, setDate) = remember { mutableStateOf(LocalDate.now()) }
            DateField(Modifier.fillMaxWidth(), date = date, setDate = setDate)
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dateField_dateButton))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .assertExists()
    }

    @OptIn(ExperimentalComposeUiApi::class)
    @Test
    fun datePicker_shouldOpen_onDateFieldFocus() {
        val now = LocalDate.now()
        composeTestRule.setContent {
            val (textFocus, dateFocus) = FocusRequester.createRefs()

            Column {
                TextField(
                    value = "Placeholder",
                    onValueChange = {},
                    modifier = Modifier
                        .focusTarget()
                        .focusOrder(textFocus) {
                            next = dateFocus
                            down = dateFocus
                        },
                    keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Next)
                )
                val (date, setDate) = remember { mutableStateOf(now) }
                DateField(
                    Modifier
                        .fillMaxWidth()
                        .focusTarget()
                        .focusOrder(dateFocus) {
                            previous = textFocus
                            up = textFocus
                        }, date = date, setDate = setDate
                )
            }
        }

        composeTestRule.onNodeWithText("Placeholder")
            .performClick()
            .performImeAction()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dateField_textField))
            .assertIsFocused()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .assertExists()
    }

    @Test
    fun datePicker_shouldOpen_onDateFieldClick() {
        val now = LocalDate.now()
        composeTestRule.setContent {
            val (date, setDate) = remember { mutableStateOf(now) }
            DateField(Modifier.fillMaxWidth(), date = date, setDate = setDate)
        }

        composeTestRule.onNodeWithText(
            DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).format(now)
        )
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .assertExists()
    }

    @Test
    fun dateDialog_shouldUpdateDate_whenChoosingNewValues() {
        val today = LocalDate.now()
        val futureDate = LocalDate.of(
            today.year + 1,
            if (today.monthValue == 1) 2 else 1,
            if (today.dayOfMonth == 1) 2 else 1
        )
        composeTestRule.setContent {
            val (date, setDate) = remember { mutableStateOf(today) }
            DateDialog(date = date, setDate = setDate, onCloseDialog = {})
        }

        composeTestRule.onNodeWithText(today.dayOfMonth.toString())
            .performClick()
        composeTestRule.onNodeWithText(futureDate.dayOfMonth.toString())
            .performClick()
        composeTestRule.onNodeWithText(dateFormatSymbols.months[today.monthValue - 1])
            .performClick()
        composeTestRule.onNodeWithText(dateFormatSymbols.months[futureDate.monthValue - 1])
            .performClick()
        composeTestRule.onNodeWithText(today.year.toString())
            .performClick()
        composeTestRule.onNodeWithText(futureDate.year.toString())
            .performClick()

        composeTestRule.onNodeWithText(futureDate.dayOfMonth.toString())
            .assertExists()
        composeTestRule.onNodeWithText(today.dayOfMonth.toString())
            .assertDoesNotExist()
        composeTestRule.onNodeWithText(dateFormatSymbols.months[futureDate.monthValue - 1])
            .assertExists()
        composeTestRule.onNodeWithText(dateFormatSymbols.months[today.monthValue - 1])
            .assertDoesNotExist()
        composeTestRule.onNodeWithText(futureDate.year.toString())
            .assertExists()
        composeTestRule.onNodeWithText(today.year.toString())
            .assertDoesNotExist()
    }

    @Test
    fun dateField_shouldNotReact_whenDisabled() {
        composeTestRule.setContent {
            DateField(
                Modifier.fillMaxWidth(),
                date = LocalDate.now(),
                setDate = {},
                disabled = true
            )
        }

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.dateField_dateButton))
            .assertDoesNotExist()
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dateField_textField))
            .performClick()
            .assertIsNotEnabled()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .assertDoesNotExist()

    }
}