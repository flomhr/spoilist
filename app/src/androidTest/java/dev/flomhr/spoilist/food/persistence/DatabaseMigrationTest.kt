/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.food.persistence

import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.platform.app.InstrumentationRegistry
import dev.flomhr.spoilist.MIGRATION_1_2
import dev.flomhr.spoilist.SpoilistDatabase
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import java.io.IOException

class DatabaseMigrationTest {

    private val TEST_DB = "spoilist-test"

    @get:Rule
    val helper: MigrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        SpoilistDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    @Throws(IOException::class)
    fun migrate1To2() {
        var db = helper.createDatabase(TEST_DB, 1)

        db.apply {
            execSQL("INSERT INTO food_items VALUES ('1234', 'Test', '01/01/2021', false, 'Test Category')")
            close()
        }

        db = helper.runMigrationsAndValidate(TEST_DB, 2, true, MIGRATION_1_2)

        val cursor = db.query("SELECT * FROM food_items WHERE id = '1234'")
        cursor.moveToFirst()
        assertEquals(
            "storage",
            cursor.getColumnName(5)
        )
        assertEquals(
            1,
            cursor.count
        )
        assertEquals(
            "",
            cursor.getString(5)
        )
    }
}