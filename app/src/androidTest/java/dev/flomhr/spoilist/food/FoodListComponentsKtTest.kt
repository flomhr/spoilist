package dev.flomhr.spoilist.food

import androidx.activity.ComponentActivity
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.food.persistence.CategoryGrouping
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.food.persistence.FoodListSorting
import dev.flomhr.spoilist.food.persistence.NoneGrouping
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

class FoodListComponentsKtTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun foodList_shouldDisplayItems() {
        val items = mapOf(
            "" to listOf(
                FoodItem("1", "Item 1", LocalDate.now()),
                FoodItem("2", "Item 2", LocalDate.now()),
                FoodItem("3", "Item 3", LocalDate.now())
            )
        )
        composeTestRule.setContent {
            FoodList(items = items, onFoodPressed = {})
        }

        // Workaround because onNodeWithText does not find "Item 1"
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertCountEquals(3)
    }

    @Test
    fun foodListScreen_shouldShowSortingMenu_onSortActionClick() {
        composeTestRule.setContent {
            FoodListScreen(
                foodItems = mapOf(),
                onFoodClicked = { },
                onAddClicked = { },
                currentSorting = FoodListSorting(
                    FoodListSorting.Field.DATE,
                    FoodListSorting.Direction.ASCENDING
                ),
                onSortingChanged = {},
                currentGrouping = NoneGrouping,
                onGroupingChanged = {}
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.sortMenu_title))
            .assertExists()
    }

    @Test
    fun sortMenu_shouldShowCurrentSorting() {
        composeTestRule.setContent {
            FoodListScreen(
                foodItems = mapOf(),
                onFoodClicked = { },
                onAddClicked = { },
                currentSorting = FoodListSorting(
                    FoodListSorting.Field.NAME,
                    FoodListSorting.Direction.DESCENDING
                ),
                onSortingChanged = {},
                currentGrouping = NoneGrouping,
                onGroupingChanged = {}
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithTag(
            composeTestRule.activity.resources.getString(
                R.string.sortMenu_sortButton,
                FoodListSorting.Field.NAME,
                FoodListSorting.Direction.DESCENDING
            )
        )
            .assertIsSelected()
    }

    @Test
    fun sortMenu_shouldShowCurrentGrouping() {
        composeTestRule.setContent {
            FoodListScreen(
                foodItems = mapOf(),
                onFoodClicked = { },
                onAddClicked = { },
                currentSorting = FoodListSorting(
                    FoodListSorting.Field.NAME,
                    FoodListSorting.Direction.DESCENDING
                ),
                onSortingChanged = {},
                currentGrouping = CategoryGrouping,
                onGroupingChanged = {}
            )
        }

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.grouping_category))
            .assertExists()
    }

    @Test
    fun foodList_shouldShowGroupingHeaders_onSelectedGrouping() {
        val category1 = "Category 1"
        val category2 = "Category 2"
        val items = mapOf(
            category1 to listOf(
                FoodItem("1", "Item 1", LocalDate.now(), category = category1),
                FoodItem("2", "Item 2", LocalDate.now(), category = category1),
            ),
            category2 to listOf(
                FoodItem("3", "Item 3", LocalDate.now(), category = category2)
            )
        )

        composeTestRule.setContent {
            FoodListScreen(
                foodItems = items,
                onFoodClicked = { },
                onAddClicked = { },
                currentSorting = FoodListSorting(
                    FoodListSorting.Field.NAME,
                    FoodListSorting.Direction.DESCENDING
                ),
                onSortingChanged = {},
                currentGrouping = CategoryGrouping,
                onGroupingChanged = {}
            )
        }

        composeTestRule.onNodeWithText(category1)
            .assertExists()
        composeTestRule.onNodeWithText(category2)
            .assertExists()
    }
}