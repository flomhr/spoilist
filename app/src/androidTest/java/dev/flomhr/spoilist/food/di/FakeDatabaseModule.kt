/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.food.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import dev.flomhr.spoilist.SpoilistDatabase
import dev.flomhr.spoilist.food.persistence.FoodItemDao
import dev.flomhr.spoilist.food.persistence.FoodItemRepository
import dev.flomhr.spoilist.food.persistence.IFoodItemRepository
import javax.inject.Singleton

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [DatabaseModule::class]
)
object FakeDatabaseModule {

    @Provides
    fun provideFoodItemRepository(foodItemDao: FoodItemDao): IFoodItemRepository {
        return FoodItemRepository(foodItemDao = foodItemDao)
    }

    @Provides
    fun provideFoodItemDao(database: SpoilistDatabase): FoodItemDao {
        return database.foodItemDao()
    }

    @Provides
    @Singleton
    fun provideSpoilistDatabase(@ApplicationContext applicationContext: Context): SpoilistDatabase {
        return Room.inMemoryDatabaseBuilder(
            applicationContext,
            SpoilistDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()
    }

}