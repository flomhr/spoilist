/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.food

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.food.persistence.FoodItem
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

class FoodDetailComponentsKtTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun foodForm_shouldSwitchToEditMode_onEditClicked() {
        val foodItem = FoodItem("1", "Test Food", LocalDate.now())
        composeTestRule.setContent {
            FoodDetailScreen(
                foodItem = foodItem,
                onCloseScreen = {},
                onDeleteFoodItem = {},
                onUpdateFoodItem = {}
            )
        }

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_editBtn))
            .performClick()

        composeTestRule.onNodeWithText(foodItem.name)
            .assertIsEnabled()

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_editBtn))
            .assertDoesNotExist()

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_saveBtn))
            .assertExists()
    }

    @Test
    fun foodForm_shouldUpdateFoodItem_onSaveClick() {
        var foodItem = FoodItem("1", "Test Food", LocalDate.of(2021, 1, 1))
        composeTestRule.setContent {
            FoodDetailScreen(
                foodItem = foodItem,
                onCloseScreen = {},
                onDeleteFoodItem = {},
                onUpdateFoodItem = {
                    foodItem = it
                }
            )
        }
        val newFoodItem = foodItem.copy(name = "New Food", spoilDate = LocalDate.of(2021, 1, 2))

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_editBtn))
            .performClick()

        composeTestRule.onNodeWithText(foodItem.name)
            .performTextReplacement(newFoodItem.name)
        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.spoilDateButton_description))
            .performClick()
        composeTestRule.onNodeWithText(foodItem.spoilDate.dayOfMonth.toString())
            .performClick()
        composeTestRule.onNodeWithText(newFoodItem.spoilDate.dayOfMonth.toString())
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .performClick()

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_saveBtn))
            .performClick()

        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_editBtn))
            .assertExists()

        assertEquals(newFoodItem.name, foodItem.name)
        assertEquals(newFoodItem.spoilDate, foodItem.spoilDate)
    }

}