/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.shared

import androidx.activity.ComponentActivity
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dev.flomhr.spoilist.R
import org.junit.Rule
import org.junit.Test

class SharedComponentsTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun dropdownMenu_shouldOpen_onDropdownFieldClick() {
        composeTestRule.setContent {
            val (day, setDay) = remember { mutableStateOf(1) }
            DropdownField(
                initialValue = day,
                valueOptions = listOf(1, 2, 3),
                onValuePicked = { setDay(it) })
        }

        composeTestRule.onNodeWithText("1")
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dropdownField_dropdownMenu))
            .assertExists()
    }

    @Test
    fun dropdownField_shouldDisplayNewValue_onDropdownSelect() {
        composeTestRule.setContent {
            val (day, setDay) = remember { mutableStateOf(1) }
            DropdownField(
                initialValue = day,
                valueOptions = listOf(1, 2, 3),
                onValuePicked = { setDay(it) })
        }

        composeTestRule.onNodeWithText("1")
            .assertExists()
        composeTestRule.onNodeWithText("1")
            .performClick()
        composeTestRule.onNodeWithText("3")
            .performClick()

        composeTestRule.onNodeWithText("3")
            .assertExists()

    }

    @Test
    fun dropdownField_shouldHideDropdownMenu_onDropdownSelect() {
        composeTestRule.setContent {
            val (day, setDay) = remember { mutableStateOf(1) }
            DropdownField(
                initialValue = day,
                valueOptions = listOf(1, 2, 3),
                onValuePicked = { setDay(it) })
        }

        composeTestRule.onNodeWithText("1")
            .assertExists()

        composeTestRule.onNodeWithText("1")
            .performClick()
        composeTestRule.onNodeWithText("3")
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dropdownField_dropdownMenu))
            .assertDoesNotExist()

    }

    @Test
    fun dropdownMenu_shouldDisplayMoreValues_onScroll() {
        composeTestRule.setContent {
            val (day, setDay) = remember { mutableStateOf(1) }
            DropdownField(
                initialValue = day,
                valueOptions = (1..31).toList(),
                onValuePicked = { setDay(it) })
        }

        composeTestRule.onNodeWithText("1")
            .performClick()
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.dropdownField_dropdownMenu))
            .performGesture {
                swipeUp()
                swipeUp()
            }
        composeTestRule.onNodeWithText("18")
            .performClick()

        composeTestRule.onNodeWithText("18")
            .assertExists()
    }

    @Test
    fun dropdownField_shouldDisplayCorrectValue_whenSpecifyingFormatting() {
        composeTestRule.setContent {
            DropdownField(
                initialValue = 1,
                valueOptions = listOf(1, 2, 3),
                onValuePicked = { },
                valueFormatting = {
                    "!$it!"
                })
        }

        composeTestRule.onNodeWithText("!1!")
            .assertExists()
    }

}