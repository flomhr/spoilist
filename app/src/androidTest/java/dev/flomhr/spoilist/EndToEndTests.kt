package dev.flomhr.spoilist

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.food.persistence.FoodItemDao
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate
import javax.inject.Inject

@HiltAndroidTest
class EndToEndTests {

    @get:Rule(order = 1)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Inject
    lateinit var foodItemDao: FoodItemDao

    @Before
    fun inject() {
        hiltRule.inject()
    }

    @Test
    fun foodItem_canBeAdded_and_isDisplayedInTheList(): Unit = runBlocking {
        val testFood = FoodItem("1", "Test Food", LocalDate.now())

        // Open add food screen and create a valid food item
        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_addFoodBtn))
            .performClick()
        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodForm_nameField))
            .performClick()
            .performTextInput(testFood.name)
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.addFoodScreen_doneBtn))
            .performClick()
        // Check if activity was switched back and an item is displayed
        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.addFoodScreen_title))
            .assertDoesNotExist()
        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_column))
            .onChildren()
            .assertCountEquals(1)
        // Check if item in database matches created item
        val foodList = foodItemDao.getFoodList().first()
        assertEquals(1, foodList.size)
        val expectedFood = testFood.copy(id = foodList[0].id)
        assertEquals(expectedFood, foodList[0])
    }

    @Test
    fun foodItem_canBeDeleted_and_deleteUndone(): Unit = runBlocking {
        val foods = listOf(
            FoodItem("1", "Item 1", LocalDate.now().plusDays(2)),
            FoodItem("2", "Item 2", LocalDate.now().minusDays(2)),
            FoodItem("3", "Item 3", LocalDate.now())
        )

        foodItemDao.insertFood(foods[0])
        foodItemDao.insertFood(foods[1])
        foodItemDao.insertFood(foods[2])

        // Open food item and delete it
        composeTestRule.onNodeWithText(foods[0].name)
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.foodDetail_deleteBtn))
            .performClick()
        // Check if item was deleted and snackbar is shown
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertCountEquals(2)
        composeTestRule.onNodeWithText(foods[0].name)
            .assertDoesNotExist()
        composeTestRule.onNodeWithText(
            composeTestRule.activity.getString(
                R.string.deletedFood_info,
                foods[0].name
            )
        )
            .assertExists()
        // Check if item was removed from the database
        var foodList = foodItemDao.getFoodList().first()
        assertEquals(2, foodList.size)
        assertFalse(foodList.contains(foods[0]))
        // Press Undo button on snackbar
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.undo))
            .performClick()
        // Check if item was restored and snackbar is gone
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertCountEquals(3)
        composeTestRule.onNodeWithText(foods[0].name)
            .assertExists()
        composeTestRule.onNodeWithText(
            composeTestRule.activity.getString(
                R.string.deletedFood_info,
                foods[0].name
            )
        )
            .assertDoesNotExist()
        // Check if all items are back in the database
        foodList = foodItemDao.getFoodList().first()
        assertEquals(3, foodList.size)
        assertTrue(foodList.contains(foods[0]))
    }

    @Test
    fun foodItem_canBeEdited_and_isUpdatedInFoodList(): Unit = runBlocking {
        val foods = listOf(
            FoodItem("1", "Item 1", LocalDate.of(2021, 1, 4)),
            FoodItem("2", "Item 2", LocalDate.of(2021, 1, 2)),
            FoodItem("3", "Item 3", LocalDate.of(2021, 1, 3))
        )

        val newFoodItem = foods[0].copy(name = "New Item", spoilDate = LocalDate.of(2021, 1, 1))

        foodItemDao.insertFood(foods[0])
        foodItemDao.insertFood(foods[1])
        foodItemDao.insertFood(foods[2])

        // Open food item and ...
        composeTestRule.onNodeWithText(foods[0].name)
            .performClick()
        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_editBtn))
            .performClick()
        // ... change name ...
        composeTestRule.onNodeWithText(foods[0].name)
            .performTextReplacement(newFoodItem.name)
        // ... change date ...
        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.spoilDateButton_description))
            .performClick()
        composeTestRule.onNodeWithText(foods[0].spoilDate.dayOfMonth.toString())
            .performClick()
        composeTestRule.onNodeWithText(newFoodItem.spoilDate.dayOfMonth.toString())
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(android.R.string.ok))
            .performClick()
        // ... save changes
        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetail_saveBtn))
            .performClick()
        composeTestRule.onNodeWithContentDescription(composeTestRule.activity.getString(R.string.foodDetails_backBtn))
            .performClick()
        // Check if food list was updated
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.foodDetails_title))
            .assertDoesNotExist()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.app_name))
            .assertExists()
        composeTestRule.onNodeWithText(foods[0].name)
            .assertDoesNotExist()
        composeTestRule.onNodeWithText(newFoodItem.name)
            .assertExists()
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(0)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        newFoodItem.name
                    )
                )
            )
        // Check if item was updated in database
        val foodList = foodItemDao.getFoodList().first()
        assertTrue(foodList.contains(newFoodItem))
        assertFalse(foodList.contains(foods[0]))
    }

}