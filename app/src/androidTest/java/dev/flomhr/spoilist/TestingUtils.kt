package dev.flomhr.spoilist

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import dev.flomhr.spoilist.food.*
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.food.persistence.IFoodItemRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class TestingFoodItemRepository : IFoodItemRepository {

    private val foodListData = mutableListOf<FoodItem>()
    private val foodListFlow: MutableStateFlow<List<FoodItem>> = MutableStateFlow(listOf())
    override val foodList: Flow<List<FoodItem>>
        get() = foodListFlow

    override suspend fun insertFood(foodItem: FoodItem) {
        foodListData += foodItem
        foodListFlow.emit(foodListData)
    }

    override suspend fun removeFood(foodItem: FoodItem) {
        foodListData -= foodItem
        foodListFlow.emit(foodListData)
    }

    override suspend fun updateFood(foodItem: FoodItem) {
        foodListData.find { it.id == foodItem.id }?.let { foundFood ->
            foodListData.set(foodListData.indexOf(foundFood), foodItem)
        }
    }
}

@VisibleForTesting(otherwise = VisibleForTesting.NONE)
fun <T> LiveData<T>.getOrAwaitValue(
    time: Long = 2,
    timeUnit: TimeUnit = TimeUnit.SECONDS,
    afterObserve: () -> Unit = {}
): T {
    var data: T? = null
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data = o
            latch.countDown()
            this@getOrAwaitValue.removeObserver(this)
        }
    }
    this.observeForever(observer)

    try {
        afterObserve.invoke()

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

    } finally {
        this.removeObserver(observer)
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}

