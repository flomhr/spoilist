package dev.flomhr.spoilist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dev.flomhr.spoilist.food.FoodListViewModel
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.food.persistence.FoodListSorting
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.LocalDate

@HiltAndroidTest
class MainActivityKtTest {

    @get:Rule(order = 1)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @get:Rule(order = 3)
    var instantExecutorRule = InstantTaskExecutorRule()

    @BindValue
    var viewModel = FoodListViewModel(TestingFoodItemRepository())

    @Before
    fun inject() {
        hiltRule.inject()
    }

    @Test
    fun activity_shouldSwitchToAddScreen_onAddButtonClick() {
        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_addFoodBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.addFoodScreen_title))
            .assertExists()
    }

    @Test
    fun foodListScreen_shouldSortList_onDateSortingPicked() {
        val foods = listOf(
            FoodItem("1", "Item 1", LocalDate.now().plusDays(2)),
            FoodItem("2", "Item 2", LocalDate.now().minusDays(2)),
            FoodItem("3", "Item 3", LocalDate.now())
        )

        viewModel.addFood(foods[0])
        viewModel.addFood(foods[1])
        viewModel.addFood(foods[2])

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.sortMenu_spoilDate_ascending))
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(0)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[1].name
                    )
                )
            )

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(1)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[2].name
                    )
                )
            )

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(2)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[0].name
                    )
                )
            )
    }

    @Test
    fun foodListScreen_shouldSortList_onNameSortingPicked() {
        val foods = listOf(
            FoodItem("1", "B Item", LocalDate.now().plusDays(2)),
            FoodItem("2", "A Item", LocalDate.now().minusDays(2)),
            FoodItem("3", "C Item", LocalDate.now())
        )

        viewModel.addFood(foods[0])
        viewModel.addFood(foods[1])
        viewModel.addFood(foods[2])

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.sortMenu_name_ascending))
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(0)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[1].name
                    )
                )
            )

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(1)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[0].name
                    )
                )
            )

        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildAt(2)
            .assert(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[2].name
                    )
                )
            )
    }

    @Test
    fun sortMenu_shouldDisplayNewSorting_afterSortingChanged() {
        val foods = listOf(
            FoodItem("1", "Item 1", LocalDate.now().plusDays(2)),
            FoodItem("2", "Item 2", LocalDate.now().minusDays(2)),
            FoodItem("3", "Item 3", LocalDate.now())
        )

        viewModel.addFood(foods[0])
        viewModel.addFood(foods[1])
        viewModel.addFood(foods[2])

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.sortMenu_spoilDate_descending))
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithTag(
            composeTestRule.activity.resources.getString(
                R.string.sortMenu_sortButton,
                FoodListSorting.Field.DATE,
                FoodListSorting.Direction.DESCENDING
            )
        )
            .assertIsSelected()
    }

    @Test
    fun activity_shouldSwitchToFoodDetailScreen_onFoodItemClick() {
        val foods = listOf(
            FoodItem("1", "Item 1", LocalDate.now().plusDays(2)),
            FoodItem("2", "Item 2", LocalDate.now().minusDays(2), category = "Category 2"),
            FoodItem("3", "Item 3", LocalDate.now())
        )

        viewModel.addFood(foods[0])
        viewModel.addFood(foods[1])
        viewModel.addFood(foods[2])

        composeTestRule.onNodeWithText(foods[1].name)
            .performClick()

        composeTestRule.onNodeWithText(foods[1].name)
            .assertExists()
        composeTestRule.onNodeWithText(foods[1].category)
            .assertExists()
        composeTestRule.onNodeWithText(foods[0].name)
            .assertDoesNotExist()
        composeTestRule.onNodeWithText(foods[2].name)
            .assertDoesNotExist()
    }

    @Test
    fun foodListScreen_shouldGroupList_onCategoryGroupingPicked() {
        val category1 = "Category 1"
        val category2 = "Category 2"
        val foods = listOf(
            FoodItem("1", "A Item", LocalDate.now(), category = category1),
            FoodItem("2", "B Item", LocalDate.now().plusDays(1), category = category2),
            FoodItem("3", "C Item", LocalDate.now().plusDays(2), category = category1)
        )

        viewModel.addFood(foods[0])
        viewModel.addFood(foods[1])
        viewModel.addFood(foods[2])

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.grouping_none))
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.grouping_category))
            .performClick()

        // Check if all headers and cards are present, checking for position does not work with headers for unknown reasons
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren().assertCountEquals(5)
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertAny(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_groupingHeader, category1
                    )
                )
            )
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertAny(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[0].name
                    )
                )
            )
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertAny(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[1].name
                    )
                )
            )
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertAny(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_groupingHeader, category2
                    )
                )
            )
        composeTestRule.onNodeWithTag(composeTestRule.activity.getString(R.string.foodList_column))
            .onChildren()
            .assertAny(
                hasTestTag(
                    composeTestRule.activity.resources.getString(
                        R.string.foodList_foodCard,
                        foods[2].name
                    )
                )
            )
    }

    @Test
    fun sortMenu_shouldDisplayNewGrouping_afterGroupingChanged() {
        val foods = listOf(
            FoodItem("1", "Item 1", LocalDate.now().plusDays(2)),
            FoodItem("2", "Item 2", LocalDate.now().minusDays(2)),
            FoodItem("3", "Item 3", LocalDate.now())
        )

        viewModel.addFood(foods[0])
        viewModel.addFood(foods[1])
        viewModel.addFood(foods[2])

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.grouping_none))
            .performClick()
        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.grouping_category))
            .performClick()

        composeTestRule.onNodeWithTag(composeTestRule.activity.resources.getString(R.string.foodList_sortActionBtn))
            .performClick()

        composeTestRule.onNodeWithText(composeTestRule.activity.resources.getString(R.string.grouping_category))
            .assertExists()
    }

}