package dev.flomhr.spoilist.util

import android.util.Log
import androidx.compose.ui.graphics.Color
import dev.flomhr.spoilist.food.FoodListViewModel
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.format.FormatStyle
import java.util.*

private const val TAG = "dev.flomhr.spoilist.util.Utils"

const val spoilSoonWarningDays = 3

fun LocalDate.getShortFormat(): String {
    return getFormattedDate(this)
}

/**
 * Formats a given date to string according to style and locale.
 * @param date The date to be formatted.
 * @param formatStyle Style of the resulting string. Default is [FormatStyle.SHORT].
 * @param locale Locale of the resulting string. Uses default locale if not specified.
 */
fun getFormattedDate(
    date: LocalDate,
    formatStyle: FormatStyle = FormatStyle.SHORT,
    locale: Locale = Locale.getDefault()
): String {
    return DateTimeFormatter
        .ofLocalizedDate(formatStyle)
        .withLocale(locale)
        .format(date)
}

/**
 * Returns the color to be used for a given spoil date.
 */
fun getColorForSpoilDate(spoilDate: LocalDate): Color {
    val today = LocalDate.now()
    val spoilSoonWarningDate = spoilDate.minusDays(spoilSoonWarningDays.toLong())

    return when {
        today.isAfter(spoilDate) -> Color.Red
        today.isAfter(spoilSoonWarningDate) || today.isEqual(spoilSoonWarningDate) -> Color.Yellow
        else -> Color.Green
    }
}


/**
 * Parses a given date text and returns a [LocalDate].
 * @param dateString The text that should be parsed. The format of the date has to match the
 * [formatStyle] of the given [locale].
 * @param formatStyle The date format that the text is in.
 * @param locale The locale to be used for parsing. Defaults to current locale.
 * @return A [LocalDate] parsed from the date text or null if parsing fails.
 */
fun getDateFromString(
    dateString: String,
    formatStyle: FormatStyle,
    locale: Locale = Locale.getDefault()
): LocalDate? {
    val formatter = DateTimeFormatter.ofLocalizedDate(formatStyle).withLocale(locale)
    return try {
        LocalDate.from(formatter.parse(dateString))
    } catch (e: DateTimeParseException) {
        Log.w(
            TAG,
            "Could not parse string $dateString with format style ${formatStyle.name}: ${e.message}"
        )
        null
    }
}

/**
 * Gets all distinct categories from the stored food items in the viewmodel.
 */
fun FoodListViewModel.getAllCategories(): List<String> {
    return foodItems.value?.let { map ->
        map.flatMap { it.value }.map { it.category }.distinct()
    } ?: listOf()
}

/**
 * Gets all distinct storage locations from the stored food items in the viewmodel.
 */
fun FoodListViewModel.getAllStorageLocations(): List<String> {
    return foodItems.value?.let { map ->
        map.flatMap { it.value }.map { it.storage }.distinct()
    } ?: listOf()
}