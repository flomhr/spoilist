package dev.flomhr.spoilist.food

import dev.flomhr.spoilist.food.persistence.Sorting

data class SortButtonState(
    val title: String,
    val sorting: Sorting,
    val selected: Boolean
)
