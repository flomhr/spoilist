package dev.flomhr.spoilist.food

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.food.persistence.*
import dev.flomhr.spoilist.shared.DialogWrapper
import dev.flomhr.spoilist.shared.DropdownField
import dev.flomhr.spoilist.ui.SpoiListTheme
import dev.flomhr.spoilist.util.getColorForSpoilDate
import dev.flomhr.spoilist.util.getFormattedDate
import java.time.LocalDate


/**
 * Displays the specified food item on a card.
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun FoodListCard(
    modifier: Modifier = Modifier,
    foodItem: FoodItem,
    onPressed: (FoodItem) -> Unit
) {
    Card(
        modifier = modifier,
        elevation = 4.dp,
        onClick = {
            onPressed(foodItem)
        }
    ) {
        Row(
            modifier = Modifier.padding(8.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Column(
                verticalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.padding(start = 4.dp)
            ) {
                Text(text = foodItem.name, modifier = Modifier.padding(bottom = 4.dp))
                Text(
                    text = getFormattedDate(foodItem.spoilDate),
                    color = getColorForSpoilDate(foodItem.spoilDate)
                )
            }

            if (foodItem.opened) {
                Row(horizontalArrangement = Arrangement.spacedBy(4.dp)) {
                    Text(text = stringResource(id = R.string.opened))
                    Icon(
                        painter = painterResource(id = R.drawable.ic_warning),
                        contentDescription = stringResource(R.string.foodCard_opened_description),
                        tint = MaterialTheme.colors.secondary
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FoodList(
    modifier: Modifier = Modifier,
    items: Map<String, List<FoodItem>>,
    onFoodPressed: (FoodItem) -> Unit
) {
    val foodListTestTag = stringResource(id = R.string.foodList_column)
    val sortedItems = remember(items) {
        items.toSortedMap { grouping1, grouping2 ->
            when {
                grouping1.isBlank() -> {
                    1
                }
                grouping2.isBlank() -> {
                    -1
                }
                else -> {
                    grouping1.compareTo(grouping2)
                }
            }
        }
    }

    LazyColumn(
        modifier = modifier
            .testTag(foodListTestTag),
        contentPadding = PaddingValues(4.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        if (items.size < 2) {
            items(items.flatMap { it.value }) { food ->
                val cardTestTag = stringResource(id = R.string.foodList_foodCard, food.name)
                FoodListCard(
                    modifier = Modifier
                        .fillMaxWidth()
                        .testTag(cardTestTag),
                    foodItem = food,
                    onPressed = onFoodPressed
                )
            }
        } else {
            sortedItems.forEach { (grouping, foods) ->

                stickyHeader(grouping) {
                    val headerText =
                        if (grouping.isNotBlank()) {
                            grouping
                        } else {
                            stringResource(id = R.string.foodList_ungroupedHeader)
                        }
                    val headerTestTag =
                        stringResource(id = R.string.foodList_groupingHeader, grouping)
                    Text(text = headerText, modifier = Modifier.testTag(headerTestTag))
                }

                items(foods) { food ->
                    val cardTestTag = stringResource(id = R.string.foodList_foodCard, food.name)
                    FoodListCard(
                        modifier = Modifier
                            .fillMaxWidth()
                            .testTag(cardTestTag),
                        foodItem = food,
                        onPressed = onFoodPressed
                    )
                }
            }
        }
    }
}

@Composable
fun FoodListScreen(
    modifier: Modifier = Modifier,
    foodItems: Map<String, List<FoodItem>>,
    onFoodClicked: (FoodItem) -> Unit,
    onAddClicked: () -> Unit,
    currentSorting: FoodListSorting,
    onSortingChanged: (Sorting) -> Unit,
    currentGrouping: FoodGrouping,
    onGroupingChanged: (FoodGrouping) -> Unit,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
) {
    var showSortDialog by remember { mutableStateOf(false) }

    val buttonTitles = listOf(
        stringResource(R.string.sortMenu_spoilDate_ascending),
        stringResource(R.string.sortMenu_spoilDate_descending),
        stringResource(R.string.sortMenu_name_ascending),
        stringResource(R.string.sortMenu_name_descending)
    )
    val buttonStates = remember(currentSorting) {
        listOf(
            SortButtonState(
                title = buttonTitles[0],
                sorting = DateSorting(FoodListSorting.Direction.ASCENDING),
                selected = currentSorting.field == FoodListSorting.Field.DATE &&
                        currentSorting.direction == FoodListSorting.Direction.ASCENDING
            ),
            SortButtonState(
                title = buttonTitles[1],
                sorting = DateSorting(FoodListSorting.Direction.DESCENDING),
                selected = currentSorting.field == FoodListSorting.Field.DATE &&
                        currentSorting.direction == FoodListSorting.Direction.DESCENDING
            ),
            SortButtonState(
                title = buttonTitles[2],
                sorting = NameSorting(FoodListSorting.Direction.ASCENDING),
                selected = currentSorting.field == FoodListSorting.Field.NAME &&
                        currentSorting.direction == FoodListSorting.Direction.ASCENDING
            ),
            SortButtonState(
                title = buttonTitles[3],
                sorting = NameSorting(FoodListSorting.Direction.DESCENDING),
                selected = currentSorting.field == FoodListSorting.Field.NAME &&
                        currentSorting.direction == FoodListSorting.Direction.DESCENDING
            )
        )
    }

    if (showSortDialog) {
        val groupings = listOf(NoneGrouping, CategoryGrouping, StorageGrouping)
        SortDialog(
            buttonStates = buttonStates,
            onDismissed = { showSortDialog = false },
            onSortingChanged = {
                onSortingChanged(it)
                showSortDialog = false
            },
            groupings = groupings,
            currentGrouping = currentGrouping,
            onGroupingChanged = {
                onGroupingChanged(it)
                showSortDialog = false
            }
        )
    }

    Scaffold(
        modifier = modifier,
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(id = R.string.app_name)) },
                elevation = 4.dp,
                actions = {
                    val sortMenuButtonTag = stringResource(id = R.string.foodList_sortActionBtn)
                    IconButton(onClick = {
                        showSortDialog = true
                    },
                        modifier = Modifier.semantics {
                            testTag = sortMenuButtonTag
                        }) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_sort),
                            contentDescription = stringResource(
                                id = R.string.foodList_sortActionDescription
                            )
                        )
                    }
                }
            )
        },
        floatingActionButton = {
            val addFoodButtonTag = stringResource(R.string.foodList_addFoodBtn)
            IconButton(
                modifier = Modifier
                    .clip(shape = CircleShape)
                    .background(MaterialTheme.colors.secondary)
                    .testTag(addFoodButtonTag),
                onClick = {
                    onAddClicked()
                }
            ) {
                val icon = painterResource(id = R.drawable.ic_add_light)
                Icon(icon, contentDescription = stringResource(R.string.addFoodFab_description))
            }
        },
        floatingActionButtonPosition = FabPosition.End
    ) { padding ->
        FoodList(modifier = Modifier.padding(padding), items = foodItems, onFoodPressed = {
            onFoodClicked(it)
        })

    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
private fun SortDialog(
    buttonStates: List<SortButtonState>,
    onDismissed: () -> Unit,
    onSortingChanged: (Sorting) -> Unit,
    groupings: List<FoodGrouping>,
    currentGrouping: FoodGrouping,
    onGroupingChanged: (FoodGrouping) -> Unit
) {
    DialogWrapper(
        title = stringResource(R.string.sortMenu_title),
        content = {
            SortDialogContent(
                buttonStates = buttonStates,
                onSortingChanged = onSortingChanged,
                groupings = groupings,
                currentGrouping = currentGrouping,
                onGroupingChanged = onGroupingChanged
            )
        },
        onDismissed = {
            onDismissed()
        },
        properties = DialogProperties(dismissOnClickOutside = true, usePlatformDefaultWidth = true)
    )
}

@Composable
private fun SortDialogContent(
    buttonStates: List<SortButtonState>,
    onSortingChanged: (Sorting) -> Unit,
    groupings: List<FoodGrouping>,
    currentGrouping: FoodGrouping,
    onGroupingChanged: (FoodGrouping) -> Unit,
) {

    Column {
        Text(
            modifier = Modifier.padding(bottom = 4.dp),
            text = stringResource(R.string.sortDialog_sortBy),
            style = MaterialTheme.typography.h6
        )
        Row(
            horizontalArrangement = Arrangement.spacedBy(32.dp),
            modifier = Modifier
                .padding(start = 4.dp, bottom = 8.dp)
                .fillMaxWidth()
        ) {
            Column(verticalArrangement = Arrangement.spacedBy(4.dp)) {
                for (i in buttonStates.indices step 2) {
                    SortButton(buttonStates[i], onSortingChanged)
                }
            }

            Column(verticalArrangement = Arrangement.spacedBy(4.dp)) {
                for (i in 1..buttonStates.lastIndex step 2) {
                    SortButton(buttonStates[i], onSortingChanged)
                }
            }
        }
        Text(
            modifier = Modifier.padding(bottom = 4.dp),
            text = stringResource(R.string.sortDialog_groupBy),
            style = MaterialTheme.typography.h6
        )

        val map = groupings.map { it to stringResource(id = it.titleId) }

        DropdownField(
            initialValue = currentGrouping to stringResource(id = currentGrouping.titleId),
            valueOptions = map,
            onValuePicked = {
                onGroupingChanged(it.first)
            },
            valueFormatting = {
                it.second
            }
        )
    }
}

@Composable
private fun SortButton(
    buttonState: SortButtonState,
    onSortingChanged: (Sorting) -> Unit
) {
    Row {
        RadioButton(
            modifier = Modifier.testTag(
                stringResource(
                    id = R.string.sortMenu_sortButton,
                    buttonState.sorting.sortingInfo.field,
                    buttonState.sorting.sortingInfo.direction
                )
            ),
            selected = buttonState.selected,
            onClick = {
                onSortingChanged(buttonState.sorting)
            })
        Text(
            text = buttonState.title,
            modifier = Modifier
                .padding(start = 4.dp)
                .clickable {
                    onSortingChanged(buttonState.sorting)
                }
        )
    }
}

@Preview(showBackground = true, group = "Component")
@Composable
fun PreviewFoodListCard() {
    val foodItem = FoodItem("1234", "Apples", LocalDate.of(2021, 1, 1), false, "Produce")
    SpoiListTheme {
        FoodListCard(modifier = Modifier.fillMaxWidth(), foodItem = foodItem, onPressed = {})
    }
}

@Preview(showBackground = true, group = "Component")
@Composable
fun PreviewFoodList() {
    val apples = FoodItem("1234", "Apples 🍎🍏", LocalDate.of(2021, 1, 1), false, "Produce")
    val bread = FoodItem("5678", "Bread 🍞", LocalDate.of(2021, 9, 25), true, "Frozen Food")
    val coffee = FoodItem("9101", "Coffee ☕", LocalDate.of(2021, 1, 5), false, "Drinks")

    SpoiListTheme {
        FoodList(items = mapOf("" to listOf(apples, coffee, bread)), onFoodPressed = {})
    }
}

@Preview(showBackground = true, group = "Component")
@Composable
fun PreviewFoodListGrouped() {
    val apples = FoodItem("1234", "Apples 🍎🍏", LocalDate.of(2021, 1, 1), false, "Produce")
    val bananas = FoodItem("5678", "Bananas 🍌", LocalDate.of(2021, 9, 25), true, "Produce")
    val coffee = FoodItem("9101", "Coffee ☕", LocalDate.of(2021, 1, 5), false, "Drinks")
    val donuts = FoodItem("1213", "Donuts 🍩", LocalDate.of(2021, 1, 5), false)

    SpoiListTheme {
        FoodList(
            items = mapOf(
                "Produce" to listOf(apples, bananas),
                "Drinks" to listOf(coffee),
                "" to listOf(donuts)
            ),
            onFoodPressed = {})
    }
}

@Preview(showBackground = true, group = "Screen")
@Composable
fun PreviewFoodListScreen() {
    val apples = FoodItem("1234", "Apples 🍎🍏", LocalDate.of(2021, 1, 1), false, "Produce")
    val bread = FoodItem("5678", "Bread 🍞", LocalDate.of(2021, 9, 25), true, "Frozen Food")
    val coffee = FoodItem("9101", "Coffee ☕", LocalDate.of(2021, 1, 5), false, "Drinks")

    SpoiListTheme {
        FoodListScreen(
            foodItems = mapOf("" to listOf(apples, coffee, bread)),
            onFoodClicked = {},
            onAddClicked = {},
            currentSorting = FoodListSorting(
                FoodListSorting.Field.DATE,
                FoodListSorting.Direction.ASCENDING
            ),
            onSortingChanged = {},
            currentGrouping = NoneGrouping,
            onGroupingChanged = {}
        )
    }
}

@Preview(showBackground = true, group = "Screen")
@Composable
fun PreviewFoodListScreenDark() {
    val apples = FoodItem("1234", "Apples 🍎🍏", LocalDate.of(2021, 1, 1), false, "Produce")
    val bread = FoodItem("5678", "Bread 🍞", LocalDate.of(2021, 9, 25), true, "Frozen Food")
    val coffee = FoodItem("9101", "Coffee ☕", LocalDate.of(2021, 1, 5), false, "Drinks")

    SpoiListTheme(darkTheme = true) {
        FoodListScreen(
            foodItems = mapOf("" to listOf(apples, coffee, bread)),
            onFoodClicked = {},
            onAddClicked = {},
            currentSorting = FoodListSorting(
                FoodListSorting.Field.DATE,
                FoodListSorting.Direction.ASCENDING
            ),
            onSortingChanged = {},
            currentGrouping = NoneGrouping,
            onGroupingChanged = {}
        )
    }
}

@Preview(group = "Component", showBackground = true)
@Composable
fun PreviewSortDialogContent() {
    val buttonStates = listOf(
        SortButtonState("Date ^", DateSorting(FoodListSorting.Direction.ASCENDING), true),
        SortButtonState("Date v", DateSorting(FoodListSorting.Direction.DESCENDING), false),
        SortButtonState("Name ^", NameSorting(FoodListSorting.Direction.ASCENDING), false),
        SortButtonState("Name v", NameSorting(FoodListSorting.Direction.DESCENDING), false),
    )
    SpoiListTheme {
        SortDialogContent(
            buttonStates = buttonStates,
            onSortingChanged = {},
            groupings = listOf(NoneGrouping, CategoryGrouping),
            currentGrouping = CategoryGrouping,
            onGroupingChanged = {}
        )
    }
}


