package dev.flomhr.spoilist.food.persistence

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface FoodItemDao {

    @Query("SELECT * FROM food_items")
    fun getFoodList(): Flow<List<FoodItem>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertFood(foodItem: FoodItem)

    @Delete
    suspend fun deleteFood(foodItem: FoodItem)

    @Update
    suspend fun updateFood(foodItem: FoodItem)
}