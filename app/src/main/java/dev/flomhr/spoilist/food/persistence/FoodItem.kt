package dev.flomhr.spoilist.food.persistence

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate

/**
 * Holds all infos about a food item.
 */
@Entity(tableName = "food_items")
data class FoodItem(
    @PrimaryKey val id: String,
    val name: String,
    val spoilDate: LocalDate,
    val opened: Boolean = false,
    val category: String = "",
    val storage: String = ""
) {
    fun areRequiredFieldsSet(): Boolean =
        id.isNotBlank() && name.isNotBlank()

}