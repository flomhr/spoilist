package dev.flomhr.spoilist.food.persistence

import kotlinx.coroutines.flow.Flow

interface IFoodItemRepository {
    val foodList: Flow<List<FoodItem>>

    suspend fun insertFood(foodItem: FoodItem)

    suspend fun removeFood(foodItem: FoodItem)

    suspend fun updateFood(foodItem: FoodItem)
}