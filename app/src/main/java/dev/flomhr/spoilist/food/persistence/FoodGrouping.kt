/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.food.persistence

import dev.flomhr.spoilist.R

sealed class FoodGrouping {

    abstract val titleId: Int

    abstract fun group(items: List<FoodItem>): Map<String, List<FoodItem>>
}

object NoneGrouping : FoodGrouping() {
    override val titleId: Int
        get() = R.string.grouping_none

    override fun group(items: List<FoodItem>): Map<String, List<FoodItem>> {
        return mapOf("" to items)
    }
}

object CategoryGrouping : FoodGrouping() {
    override val titleId: Int
        get() = R.string.grouping_category

    override fun group(items: List<FoodItem>): Map<String, List<FoodItem>> {
        return items.groupBy { it.category }
    }
}

object StorageGrouping : FoodGrouping() {
    override val titleId: Int
        get() = R.string.grouping_storage

    override fun group(items: List<FoodItem>): Map<String, List<FoodItem>> {
        return items.groupBy { it.storage }
    }
}
