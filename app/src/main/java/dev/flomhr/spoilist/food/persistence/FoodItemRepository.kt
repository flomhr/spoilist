package dev.flomhr.spoilist.food.persistence

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class FoodItemRepository(private val foodItemDao: FoodItemDao) : IFoodItemRepository {

    override val foodList: Flow<List<FoodItem>> = foodItemDao.getFoodList()

    @WorkerThread
    override suspend fun insertFood(foodItem: FoodItem) {
        foodItemDao.insertFood(foodItem)
    }

    @WorkerThread
    override suspend fun removeFood(foodItem: FoodItem) {
        foodItemDao.deleteFood(foodItem)
    }

    override suspend fun updateFood(foodItem: FoodItem) {
        foodItemDao.updateFood(foodItem)
    }
}