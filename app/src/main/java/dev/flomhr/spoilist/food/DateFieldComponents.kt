package dev.flomhr.spoilist.food

import android.icu.text.DateFormatSymbols
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.shared.DialogButtons
import dev.flomhr.spoilist.shared.DialogWrapper
import dev.flomhr.spoilist.shared.DropdownField
import dev.flomhr.spoilist.ui.SpoiListTheme
import dev.flomhr.spoilist.util.getFormattedDate
import java.time.LocalDate
import java.time.format.FormatStyle

@Composable
fun DateField(
    modifier: Modifier = Modifier,
    date: LocalDate,
    setDate: (LocalDate) -> Unit,
    disabled: Boolean = false,
    textFieldColors: TextFieldColors = TextFieldDefaults.textFieldColors()
) {
    var showDialog by remember { mutableStateOf(false) }

    if (showDialog && !disabled) {
        DateDialog(date = date, setDate = setDate, onCloseDialog = {
            showDialog = false
        })
    }

    TextField(
        modifier = modifier
            .clickable(
                onClick = { showDialog = true },
                enabled = !disabled
            )
            .onFocusChanged {
                if (it.isFocused) {
                    showDialog = true
                }
            }
            .testTag(stringResource(id = R.string.dateField_textField)),
        value = getFormattedDate(date = date, formatStyle = FormatStyle.LONG),
        onValueChange = { },
        label = {
            Text(text = stringResource(R.string.spoilDate_label))
        },
        colors = textFieldColors,
        trailingIcon = {
            if (!disabled) {
                val dateButtonTestTag = stringResource(R.string.dateField_dateButton)
                IconButton(
                    onClick = { showDialog = true },
                    modifier = Modifier.testTag(dateButtonTestTag)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_calendar),
                        contentDescription = stringResource(R.string.spoilDateButton_description),
                        tint = MaterialTheme.colors.secondary
                    )
                }
            }
        },
        readOnly = true,
        enabled = !disabled
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DateDialog(
    date: LocalDate,
    setDate: (LocalDate) -> Unit,
    onCloseDialog: () -> Unit
) {
    var editedDate by remember { mutableStateOf(date) }

    DialogWrapper(
        title = stringResource(R.string.dateDialog_title),
        content = {
            DateDialogContent(date = editedDate, onDateChange = {
                editedDate = it
            })
        },
        buttons = {
            DialogButtons(
                onPositiveClicked = {
                    setDate(editedDate)
                    onCloseDialog()
                },
                positiveText = stringResource(id = android.R.string.ok),
                onNegativeClicked = {
                    onCloseDialog()
                },
                negativeText = stringResource(id = android.R.string.cancel)
            )
        },
        onDismissed = {
            onCloseDialog()
        },
        properties = DialogProperties(dismissOnClickOutside = false, usePlatformDefaultWidth = true)
    )
}

@Composable
private fun DateDialogContent(
    modifier: Modifier = Modifier,
    date: LocalDate,
    onDateChange: (LocalDate) -> Unit
) {
    Row(
        modifier = modifier,
    ) {
        val dayOfMonthDescription = stringResource(R.string.datePicker_day_description)
        DropdownField(
            modifier = Modifier
                .padding(top = 8.dp, bottom = 8.dp, start = 8.dp)
                .semantics { contentDescription = dayOfMonthDescription },
            initialValue = date.dayOfMonth,
            valueOptions = (1..date.month.length(date.isLeapYear)).toList(),
            onValuePicked = {
                val newDate = LocalDate.of(date.year, date.month, it)
                onDateChange(newDate)
            }
        )
        Spacer(modifier = Modifier.width(8.dp))
        val monthDescription = stringResource(R.string.datePicker_month_description)
        DropdownField(
            modifier = Modifier
                .padding(top = 8.dp, bottom = 8.dp, start = 8.dp)
                .semantics { contentDescription = monthDescription },
            initialValue = date.monthValue,
            valueOptions = (1..12).toList(),
            onValuePicked = {
                val newDate = LocalDate.of(date.year, it, date.dayOfMonth)
                onDateChange(newDate)
            },
            valueFormatting = {
                DateFormatSymbols.getInstance().months[it - 1]
            }
        )
        Spacer(modifier = Modifier.width(8.dp))
        val yearDescription = stringResource(R.string.datePicker_year_description)
        DropdownField(
            modifier = Modifier
                .padding(top = 8.dp, bottom = 8.dp, start = 8.dp)
                .semantics { contentDescription = yearDescription },
            initialValue = date.year,
            valueOptions = (LocalDate.now().year..LocalDate.now().year + 10).toList(),
            onValuePicked = {
                val newDate = LocalDate.of(it, date.month, date.dayOfMonth)
                onDateChange(newDate)
            }
        )
    }
}

@Preview()
@Composable
fun PreviewDateField() {
    SpoiListTheme {
        Surface(color = MaterialTheme.colors.background) {
            DateField(date = LocalDate.now(), setDate = {})
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewDateFieldDark() {
    SpoiListTheme(darkTheme = true) {
        Surface(color = MaterialTheme.colors.background) {
            DateField(date = LocalDate.now(), setDate = {})
        }
    }
}

@Preview(group = "Components")
@Composable
fun PreviewDateDialogContent() {
    SpoiListTheme {
        Surface(color = MaterialTheme.colors.background) {
            DateDialogContent(date = LocalDate.now(), onDateChange = {})
        }
    }
}

@Preview(group = "Components")
@Composable
fun PreviewDateDialogContentDark() {
    SpoiListTheme(darkTheme = true) {
        Surface(color = MaterialTheme.colors.background) {
            DateDialogContent(date = LocalDate.now(), onDateChange = {})
        }
    }
}