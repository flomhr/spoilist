package dev.flomhr.spoilist.food.persistence

import android.util.Log
import androidx.room.TypeConverter
import dev.flomhr.spoilist.util.getDateFromString
import dev.flomhr.spoilist.util.getFormattedDate
import java.time.LocalDate
import java.time.format.FormatStyle
import java.util.*

class FoodConverter {

    private val TAG = "dev.flomhr.spoilist.food.persistence.FoodConverter"

    private val formatStyle = FormatStyle.MEDIUM
    private val locale = Locale.US


    @TypeConverter
    fun dateToString(date: LocalDate): String {
        return getFormattedDate(date, formatStyle, locale)
    }

    @TypeConverter
    fun stringToDate(string: String): LocalDate {
        return getDateFromString(string, formatStyle, locale) ?: LocalDate.now().also {
            Log.w(
                TAG,
                "Saved date $string could not be parsed to a date. Returning today as fallback."
            )
        }
    }
}