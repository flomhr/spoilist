package dev.flomhr.spoilist.food

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.SoftwareKeyboardController
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.PopupProperties
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.ui.SpoiListTheme
import java.time.LocalDate
import java.util.*

@Composable
fun AddFoodScreen(
    modifier: Modifier = Modifier,
    onCloseScreen: () -> Unit,
    onConfirmFood: (FoodItem) -> Unit,
    categories: List<String>,
    storageLocations: List<String>
) {
    val (formValid, setFormValid) = remember { mutableStateOf(false) }

    Scaffold(
        modifier = modifier,
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(R.string.addFoodScreen_title)) },
                navigationIcon = {
                    IconButton(onClick = {
                        Log.d("AddFoodScreen", "User pressed close button.")
                        onCloseScreen()
                    }) {
                        val closeIcon = painterResource(R.drawable.ic_close)
                        Icon(
                            painter = closeIcon,
                            contentDescription = stringResource(android.R.string.cancel)
                        )
                    }
                },
            )
        }
    ) {
        AddFoodScreenBody(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            categories = categories,
            storageLocations = storageLocations,
            onConfirmFood = onConfirmFood,
            formValid = formValid,
            setFormValid = setFormValid
        )
    }
}

@Composable
private fun AddFoodScreenBody(
    modifier: Modifier = Modifier,
    categories: List<String>,
    storageLocations: List<String>,
    onConfirmFood: (FoodItem) -> Unit,
    formValid: Boolean,
    setFormValid: (Boolean) -> Unit
) {
    val (newFood, setNewFood) = remember {
        mutableStateOf(FoodItem(UUID.randomUUID().toString(), "", LocalDate.now()))
    }

    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        FoodForm(
            modifier = Modifier
                .padding(horizontal = 16.dp),
            foodItem = newFood,
            setFoodItem = setNewFood,
            onValidStateChanged = { valid ->
                setFormValid(valid)
            },
            categories = categories,
            storageLocations = storageLocations
        )
        if (formValid) {
            Button(
                modifier = Modifier
                    .padding(4.dp)
                    .fillMaxWidth(),
                onClick = {
                    Log.d("AddFoodScreenBody", "User pressed confirm button.")
                    onConfirmFood(newFood)
                }) {
                Text(text = stringResource(id = R.string.addFoodScreen_doneBtn))

            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun FoodForm(
    modifier: Modifier = Modifier,
    foodItem: FoodItem,
    setFoodItem: (FoodItem) -> Unit,
    onValidStateChanged: (Boolean) -> Unit = {},
    categories: List<String>,
    storageLocations: List<String>,
    disableFields: Boolean = false
) {
    val (validState, setValidState) = remember { mutableStateOf(false) }
    val newState = foodItem.areRequiredFieldsSet()
    if (validState != newState) {
        setValidState(newState)
        onValidStateChanged(newState)
    }

    val (nameFieldDirty, setNameFieldDirty) = remember { mutableStateOf(false) }

    val (nameFocus, dateFocus, categoryFocus, storageFocus) = FocusRequester.createRefs()

    DisposableEffect(true) {
        if (!disableFields) {
            nameFocus.requestFocus()
            setNameFieldDirty(false)
        }
        onDispose { }
    }

    val keyboardController = LocalSoftwareKeyboardController.current

    val textFieldColors = TextFieldDefaults.textFieldColors(
        backgroundColor = Color.Transparent,
        disabledTextColor = MaterialTheme.colors.onSurface,
        disabledLabelColor = MaterialTheme.colors.onSurface.copy(ContentAlpha.medium),
        disabledIndicatorColor = MaterialTheme.colors.onSurface.copy(alpha = TextFieldDefaults.UnfocusedIndicatorLineOpacity)
    )

    Column(modifier = modifier) {
        val nameFieldTestTag = stringResource(R.string.foodForm_nameField)
        TextField(
            modifier = Modifier
                .focusTarget()
                .focusOrder(nameFocus) {
                    next = dateFocus
                    down = dateFocus
                }
                .onFocusChanged { focusState ->
                    if (!focusState.hasFocus) {
                        setNameFieldDirty(true)
                    }
                }
                .fillMaxWidth()
                .testTag(nameFieldTestTag),
            value = foodItem.name,
            onValueChange = {
                setFoodItem(foodItem.copy(name = it))

            },
            label = { Text(text = stringResource(R.string.foodName_label)) },
            singleLine = true,
            isError = foodItem.name.isBlank() && nameFieldDirty,
            keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Next),
            colors = textFieldColors,
            enabled = !disableFields,
        )

        DateField(
            modifier = Modifier
                .focusTarget()
                .focusOrder(dateFocus) {
                    previous = nameFocus
                    up = nameFocus
                    next = categoryFocus
                    down = categoryFocus
                }
                .fillMaxWidth(),
            date = foodItem.spoilDate,
            setDate = {
                setFoodItem(foodItem.copy(spoilDate = it))
                categoryFocus.requestFocus()
            },
            disabled = disableFields,
            textFieldColors = textFieldColors
        )

        SuggestionTextField(
            modifier = Modifier.fillMaxWidth(),
            value = foodItem.category,
            onValueChange = {
                setFoodItem(foodItem.copy(category = it))
            },
            suggestions = categories,
            label = stringResource(R.string.categorySuggestion_label),
            keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Next),
            onDone = {
                keyboardController?.hide()
            },
            textFocusRequester = categoryFocus,
            textFocusOrder = {
                previous = dateFocus
                up = dateFocus
                next = storageFocus
                down = storageFocus
            },
            suggestionFieldTestTag = stringResource(R.string.categorySuggestion_textField),
            disabled = disableFields,
            textFieldColors = textFieldColors
        )

        SuggestionTextField(
            modifier = Modifier.fillMaxWidth(),
            value = foodItem.storage,
            onValueChange = {
                setFoodItem(foodItem.copy(storage = it))
            },
            suggestions = storageLocations,
            label = stringResource(R.string.storageSuggestion_label),
            keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Done),
            onDone = {
                keyboardController?.hide()
            },
            textFocusRequester = storageFocus,
            textFocusOrder = {
                previous = categoryFocus
                up = categoryFocus
            },
            suggestionFieldTestTag = stringResource(R.string.storageSuggestion_textField),
            disabled = disableFields,
            textFieldColors = textFieldColors
        )

        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                modifier = Modifier
                    .clickable(onClick = { setFoodItem(foodItem.copy(opened = !foodItem.opened)) }),
                text = stringResource(R.string.openedCheckbox_text)
            )
            val openedTestTag = stringResource(R.string.foodForm_openedCheckbox)
            Checkbox(
                modifier = Modifier
                    .testTag(openedTestTag),
                checked = foodItem.opened,
                onCheckedChange = { setFoodItem(foodItem.copy(opened = it)) },
                enabled = !disableFields
            )
        }
    }
}

@ExperimentalComposeUiApi
@Composable
fun SuggestionTextField(
    modifier: Modifier = Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    suggestions: List<String>,
    label: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    onDone: (SoftwareKeyboardController?) -> Unit = {},
    textFocusRequester: FocusRequester = FocusRequester.Default,
    textFocusOrder: FocusOrder.() -> Unit = {},
    suggestionFieldTestTag: String = stringResource(id = R.string.suggestionField_textField),
    disabled: Boolean = false,
    textFieldColors: TextFieldColors = TextFieldDefaults.textFieldColors()
) {
    var filtered by remember { mutableStateOf(listOf<String>()) }
    var blockDropdown by remember { mutableStateOf(false) }
    val controller = LocalSoftwareKeyboardController.current

    Column(modifier = modifier) {
        TextField(
            modifier = Modifier
                .focusOrder(textFocusRequester, textFocusOrder)
                .onFocusChanged { state ->
                    filtered = if (state.isFocused) {
                        suggestions.filter { it.startsWith(value, true) }
                    } else {
                        listOf()
                    }
                }
                .fillMaxWidth()
                .testTag(suggestionFieldTestTag),
            value = value,
            onValueChange = { newValue ->
                filtered = suggestions.filter { it.startsWith(newValue, true) }
                onValueChange(newValue)
                blockDropdown = false
            },
            colors = textFieldColors,
            label = { Text(text = label) },
            singleLine = true,
            keyboardOptions = keyboardOptions,
            keyboardActions = KeyboardActions(onDone = {
                onDone(controller)
            }),
            enabled = !disabled
        )

        DropdownMenu(
            modifier = Modifier.testTag(stringResource(R.string.suggestionField_suggestionCard)),
            expanded = !blockDropdown && value.isNotBlank() && filtered.isNotEmpty(),
            onDismissRequest = { blockDropdown = true },
            properties = PopupProperties()
        ) {
            filtered.forEach { suggestion ->
                DropdownMenuItem(onClick = {
                    filtered = (listOf())
                    onValueChange(suggestion)
                    blockDropdown = false
                }) {
                    Text(
                        text = suggestion,
                    )
                }
            }
        }

    }
}

@Preview(showBackground = true, group = "Screen")
@Composable
fun PreviewAddFoodScreen() {
    SpoiListTheme {
        AddFoodScreen(
            onCloseScreen = {},
            onConfirmFood = {},
            categories = listOf(),
            storageLocations = listOf()
        )
    }
}

@Preview(showBackground = true, group = "Screen")
@Composable
fun PreviewAddFoodScreenDark() {
    SpoiListTheme(darkTheme = true) {
        AddFoodScreen(
            onCloseScreen = {},
            onConfirmFood = {},
            categories = listOf(),
            storageLocations = listOf()
        )
    }
}

@Preview(showBackground = true, group = "Components", heightDp = 600)
@Composable
fun PreviewAddFoodScreenBody() {
    SpoiListTheme {
        AddFoodScreenBody(
            categories = listOf(),
            storageLocations = listOf(),
            onConfirmFood = {},
            formValid = true,
            setFormValid = {}
        )
    }
}

@Preview(showBackground = true, group = "Components", heightDp = 600)
@Composable
fun PreviewAddFoodScreenBodyDark() {
    SpoiListTheme(darkTheme = true) {
        Surface {
            AddFoodScreenBody(
                categories = listOf(),
                storageLocations = listOf(),
                onConfirmFood = {},
                formValid = true,
                setFormValid = {}
            )
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Preview(showBackground = true, group = "Components")
@Composable
fun PreviewSuggestionTextField() {

    val (text, setText) = remember { mutableStateOf("") }

    SpoiListTheme {
        Column(modifier = Modifier.fillMaxWidth()) {
            SuggestionTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                value = text,
                onValueChange = setText,
                suggestions = listOf("Produce", "Pasta", "Meat", "Desserts"),
                label = "Categories"
            )
        }
    }
}

