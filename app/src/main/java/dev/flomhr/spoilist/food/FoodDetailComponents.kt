package dev.flomhr.spoilist.food

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.ui.SpoiListTheme
import java.time.LocalDate

@Composable
fun FoodDetailScreen(
    modifier: Modifier = Modifier,
    foodItem: FoodItem,
    onCloseScreen: () -> Unit,
    onDeleteFoodItem: (FoodItem) -> Unit,
    onUpdateFoodItem: (FoodItem) -> Unit
) {
    var editEnabled by remember { mutableStateOf(false) }
    val (editedFood, setEditedFood) = remember(foodItem) { mutableStateOf(foodItem) }

    Scaffold(
        modifier = modifier.fillMaxHeight(),
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(R.string.foodDetails_title))
                },
                navigationIcon = {
                    IconButton(onClick = onCloseScreen) {
                        Icon(
                            Icons.Default.ArrowBack,
                            contentDescription = stringResource(R.string.foodDetails_backBtn)
                        )
                    }
                },
                actions = {
                    if (editEnabled) {
                        IconButton(onClick = {
                            editEnabled = false
                            if (editedFood != foodItem) {
                                onUpdateFoodItem(editedFood)
                            }
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_save),
                                contentDescription = stringResource(id = R.string.foodDetail_saveBtn)
                            )
                        }
                    } else {
                        IconButton(onClick = {
                            editEnabled = true
                        }) {

                            Icon(
                                painter = painterResource(id = R.drawable.ic_edit),
                                contentDescription = stringResource(id = R.string.foodDetail_editBtn)
                            )
                        }
                    }
                }
            )
        }
    ) { paddingValues ->
        FoodDetail(
            modifier = modifier
                .padding(8.dp)
                .fillMaxSize()
                .padding(paddingValues),
            foodItem = editedFood,
            deleteFoodItem = onDeleteFoodItem,
            setFoodItem = setEditedFood,
            editMode = editEnabled
        )
    }
}

@Composable
fun FoodDetail(
    modifier: Modifier = Modifier,
    foodItem: FoodItem,
    setFoodItem: (FoodItem) -> Unit,
    deleteFoodItem: (FoodItem) -> Unit,
    editMode: Boolean
) {
    Column(modifier = modifier, verticalArrangement = Arrangement.SpaceBetween) {
        FoodForm(
            modifier = Modifier.fillMaxWidth(),
            foodItem = foodItem,
            setFoodItem = setFoodItem,
            categories = listOf(),
            storageLocations = listOf(),
            disableFields = !editMode
        )

        if (MaterialTheme.colors.isLight) {
            OutlinedButton(
                onClick = { deleteFoodItem(foodItem) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 4.dp),
                colors = ButtonDefaults.outlinedButtonColors(contentColor = MaterialTheme.colors.error)
            ) {
                Text(text = stringResource(R.string.foodDetail_deleteBtn))
            }
        } else {
            Button(
                onClick = { deleteFoodItem(foodItem) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 4.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.error)
            ) {
                Text(text = stringResource(R.string.foodDetail_deleteBtn))
            }
        }
    }
}

@Composable
fun TextWithTitle(
    modifier: Modifier = Modifier,
    text: String,
    title: String
) {
    Column(modifier = modifier) {
        Text(text = title, style = MaterialTheme.typography.subtitle2)
        if (text.isNotEmpty()) {
            Text(text = text, modifier = Modifier.padding(start = 8.dp))
        } else {
            Text(
                text = "-",
                modifier = Modifier.padding(start = 8.dp),
                style = MaterialTheme.typography.body1.copy(color = Color.LightGray)
            )
        }
    }
}

@Preview(group = "Screen")
@Composable
fun PreviewFoodDetailScreen() {
    SpoiListTheme {
        FoodDetailScreen(
            foodItem = FoodItem(
                "1", "Test Food",
                spoilDate = LocalDate.now().plusDays(5),
                opened = false,
                category = "",
                storage = "Fridge"
            ),
            onCloseScreen = {},
            onDeleteFoodItem = {},
            onUpdateFoodItem = {}
        )
    }
}

@Preview(group = "Screen")
@Composable
fun PreviewFoodDetailScreenDark() {
    SpoiListTheme(darkTheme = true) {
        FoodDetailScreen(
            foodItem = FoodItem(
                "1", "Test Food",
                spoilDate = LocalDate.now().plusDays(5),
                opened = false,
                category = "",
                storage = "Fridge"
            ),
            onCloseScreen = {},
            onDeleteFoodItem = {},
            onUpdateFoodItem = {}
        )
    }
}

@Preview(group = "Components", showBackground = true, heightDp = 400)
@Composable
fun PreviewFoodDetail() {
    SpoiListTheme {
        FoodDetail(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 4.dp),
            foodItem = FoodItem(
                "1", "Test Food",
                spoilDate = LocalDate.now().plusDays(5),
                opened = false,
                category = "",
                storage = "Fridge"
            ),
            deleteFoodItem = {},
            setFoodItem = {},
            editMode = false
        )
    }
}

@Preview(group = "Components", showBackground = true)
@Composable
fun PreviewTextWithTitle() {
    SpoiListTheme {
        TextWithTitle(text = "Test text", title = "Test Title")
    }
}
