package dev.flomhr.spoilist.food.persistence

sealed class Sorting(
    val sortingInfo: FoodListSorting,
    val block: (List<FoodItem>) -> List<FoodItem>
)

class DefaultSorting(
    direction: FoodListSorting.Direction
) : Sorting(
    FoodListSorting(FoodListSorting.Field.DEFAULT, direction),
    { list ->
        if (direction == FoodListSorting.Direction.ASCENDING) {
            list.sortedBy { it.id }
        } else {
            list.sortedByDescending { it.id }
        }
    }
)

class DateSorting(
    direction: FoodListSorting.Direction
) : Sorting(
    FoodListSorting(FoodListSorting.Field.DATE, direction),
    { list ->
        if (direction == FoodListSorting.Direction.ASCENDING) {
            list.sortedBy { it.name }.sortedBy { it.spoilDate }
        } else {
            list.sortedBy { it.name }.sortedByDescending { it.spoilDate }
        }
    }
)

class NameSorting(
    direction: FoodListSorting.Direction
) : Sorting(
    FoodListSorting(FoodListSorting.Field.NAME, direction),
    { list ->
        if (direction == FoodListSorting.Direction.ASCENDING) {
            list.sortedBy { it.spoilDate }.sortedBy { it.name }
        } else {
            list.sortedBy { it.spoilDate }.sortedByDescending { it.name }
        }
    }
)

class FoodListSorting(
    val field: Field,
    val direction: Direction
) {

    enum class Field {
        DEFAULT,
        DATE,
        NAME
    }

    enum class Direction {
        ASCENDING,
        DESCENDING
    }

}
