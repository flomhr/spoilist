package dev.flomhr.spoilist.food

import android.util.Log
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.flomhr.spoilist.food.persistence.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FoodListViewModel @Inject constructor(
    private val repository: IFoodItemRepository
) : ViewModel() {

    private val defaultSorting = DateSorting(FoodListSorting.Direction.ASCENDING)
    private val defaultGrouping = NoneGrouping

    private val sorting: MutableStateFlow<Sorting> = MutableStateFlow(defaultSorting)
    private val grouping: MutableStateFlow<FoodGrouping> = MutableStateFlow(defaultGrouping)
    val foodItems: LiveData<Map<String, List<FoodItem>>> = repository.foodList
        .combine(sorting) { list, sort ->
            sort.block(list)
        }.combine(grouping) { list, grouping ->
            grouping.group(list)
        }.asLiveData()
    private val _currentSorting: MutableLiveData<FoodListSorting> =
        MutableLiveData(defaultSorting.sortingInfo)
    val currentSorting: LiveData<FoodListSorting>
        get() = _currentSorting
    private val _currentGrouping: MutableLiveData<FoodGrouping> = MutableLiveData(NoneGrouping)
    val currentGrouping: LiveData<FoodGrouping>
        get() = _currentGrouping

    private var foodForRemoval: FoodItem? = null

    fun addFood(item: FoodItem) {
        viewModelScope.launch {
            Log.i(TAG, "Adding food ${item.name}")
            repository.insertFood(item)
        }
    }

    fun removeFood(item: FoodItem) {
        viewModelScope.launch {
            Log.i(TAG, "Removing food ${item.name}")
            repository.removeFood(item)
        }
    }

    fun sortByDefault(direction: FoodListSorting.Direction) {
        viewModelScope.launch {
            Log.i(TAG, "Sorting food list by default, $direction")
            _currentSorting.value = FoodListSorting(FoodListSorting.Field.DEFAULT, direction)
            sorting.emit(DefaultSorting(direction))
        }
    }

    fun sortByDate(direction: FoodListSorting.Direction) {
        viewModelScope.launch {
            Log.i(TAG, "Sorting food list by date, $direction")
            _currentSorting.value = FoodListSorting(FoodListSorting.Field.DATE, direction)
            sorting.emit(DateSorting(direction))
        }
    }

    fun sortByName(direction: FoodListSorting.Direction) {
        viewModelScope.launch {
            Log.i(TAG, "Sorting food list by name, $direction")
            _currentSorting.value = FoodListSorting(FoodListSorting.Field.NAME, direction)
            sorting.emit(NameSorting(direction))
        }
    }

    fun markFoodForRemoval(item: FoodItem) {
        Log.i(TAG, "Marking food ${item.name} for removal")
        foodForRemoval = item
    }

    fun removeMarkedFood(): FoodItem? {
        val removedFood = foodForRemoval
        if (removedFood != null) {
            Log.i(TAG, "Removing marked food ${removedFood.name}")
            removeFood(removedFood)
        } else {
            Log.i(TAG, "No food marked for removal")
        }
        return removedFood
    }

    fun clearMarkedFood() {
        foodForRemoval = null
    }

    fun updateFoodItem(item: FoodItem) {
        viewModelScope.launch {
            Log.i(TAG, "Updating food ${item.name}")
            repository.updateFood(item)
        }
    }

    fun groupList(newGrouping: FoodGrouping) {
        viewModelScope.launch {
            Log.i(TAG, "Grouping food list by $newGrouping")
            _currentGrouping.value = newGrouping
            grouping.emit(newGrouping)
        }
    }

    companion object {
        private val TAG = FoodListViewModel::class.simpleName
    }
}
