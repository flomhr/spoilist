/*
 * Copyright (c) 2021 Florian Mairhuber.
 */

package dev.flomhr.spoilist.food.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dev.flomhr.spoilist.SpoilistDatabase
import dev.flomhr.spoilist.food.persistence.FoodItemDao
import dev.flomhr.spoilist.food.persistence.FoodItemRepository
import dev.flomhr.spoilist.food.persistence.IFoodItemRepository
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    fun provideFoodItemRepository(foodItemDao: FoodItemDao): IFoodItemRepository {
        return FoodItemRepository(foodItemDao = foodItemDao)
    }

    @Provides
    fun provideFoodItemDao(database: SpoilistDatabase): FoodItemDao {
        return database.foodItemDao()
    }

    @Provides
    @Singleton
    fun provideSpoilistDatabase(@ApplicationContext applicationContext: Context): SpoilistDatabase {
        return SpoilistDatabase.createDatabase(applicationContext)
    }

}