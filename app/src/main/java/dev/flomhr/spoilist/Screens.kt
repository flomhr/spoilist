package dev.flomhr.spoilist

enum class Screens(val route: String) {
    FoodList("foodList"),
    AddFood("addFood"),
    FoodDetail("foodDetail/{foodId}")
}