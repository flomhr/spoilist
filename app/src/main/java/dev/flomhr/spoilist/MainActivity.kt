package dev.flomhr.spoilist

import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ScaffoldState
import androidx.compose.material.SnackbarResult
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import dev.flomhr.spoilist.food.AddFoodScreen
import dev.flomhr.spoilist.food.FoodDetailScreen
import dev.flomhr.spoilist.food.FoodListScreen
import dev.flomhr.spoilist.food.FoodListViewModel
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.food.persistence.FoodListSorting
import dev.flomhr.spoilist.food.persistence.NoneGrouping
import dev.flomhr.spoilist.ui.SpoiListTheme
import dev.flomhr.spoilist.util.getAllCategories
import dev.flomhr.spoilist.util.getAllStorageLocations

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val foodListViewModel: FoodListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            SpoilistApp(foodListViewModel)
        }
    }
}

@Composable
fun SpoilistApp(foodListViewModel: FoodListViewModel) {
    val TAG = "SpoilistApp"

    val navController = rememberNavController()
    val scaffoldState = rememberScaffoldState()

    SpoiListTheme {
        val systemUiController = rememberSystemUiController()
        val scrimColor =
            if (MaterialTheme.colors.isLight) MaterialTheme.colors.primaryVariant else MaterialTheme.colors.primary

        SideEffect {
            systemUiController.setStatusBarColor(
                color = scrimColor,
                darkIcons = false
            )
        }

        NavHost(navController = navController, startDestination = Screens.FoodList.route) {
            composable(Screens.FoodList.route) {
                Log.d(TAG, "Navigated to route ${Screens.FoodList.route}")
                val removedFood = foodListViewModel.removeMarkedFood()

                if (removedFood != null) {
                    Log.i(TAG, "Got removed food ${removedFood.name}")
                    val undoText = stringResource(id = R.string.undo)
                    val deletedFoodText =
                        stringResource(id = R.string.deletedFood_info, removedFood.name)

                    LaunchedEffect(scaffoldState.snackbarHostState) {
                        Log.d(TAG, "Showing undo remove snackbar")
                        when (scaffoldState.snackbarHostState.showSnackbar(
                            message = deletedFoodText,
                            actionLabel = undoText
                        )) {
                            SnackbarResult.ActionPerformed -> {
                                Log.i(TAG, "User pressed undo for food ${removedFood.name}")
                                foodListViewModel.addFood(removedFood)
                                foodListViewModel.clearMarkedFood()
                            }
                            SnackbarResult.Dismissed -> {
                                foodListViewModel.clearMarkedFood()
                            }
                        }
                    }
                }

                FoodListActivityScreen(
                    foodListViewModel = foodListViewModel,
                    navController = navController,
                    scaffoldState = scaffoldState
                )
            }


            composable(Screens.AddFood.route) {
                Log.d(TAG, "Navigated to route ${Screens.AddFood.route}")

                AddFoodActivityScreen(
                    foodListViewModel = foodListViewModel,
                    navController = navController
                )
            }

            composable(route = Screens.FoodDetail.route) { backStackEntry ->
                Log.d(TAG, "Navigated to route ${Screens.FoodDetail.route}")

                val foodId = backStackEntry.arguments!!.getString("foodId")!!
                Log.d(TAG, "Food ID from arguments was $foodId")
                val foodItem = remember(foodId) {
                    Log.d(TAG, "Getting food item from viewmodel")
                    foodListViewModel.foodItems.value?.let { map ->
                        map.flatMap { it.value }.find { it.id == foodId }
                    }
                }
                if (foodItem != null) {
                    FoodDetailActivityScreen(
                        foodItem = foodItem,
                        foodListViewModel = foodListViewModel,
                        navController = navController,
                    )
                } else {
                    Log.w(TAG, "Cannot show detail for food with ID $foodId")
                }
            }
        }
    }
}

@Composable
fun FoodListActivityScreen(
    foodListViewModel: FoodListViewModel,
    navController: NavController,
    scaffoldState: ScaffoldState
) {
    val foodItems by foodListViewModel.foodItems.observeAsState(mapOf())
    val currentSorting by foodListViewModel.currentSorting.observeAsState(
        FoodListSorting(
            FoodListSorting.Field.DATE,
            FoodListSorting.Direction.ASCENDING
        )
    )
    val currentGrouping by foodListViewModel.currentGrouping.observeAsState(NoneGrouping)

    FoodListScreen(
        foodItems = foodItems,
        onFoodClicked = {
            navController.navigate("foodDetail/${it.id}")
        },
        onAddClicked = {
            navController.navigate(Screens.AddFood.route)
        },
        currentSorting = currentSorting,
        onSortingChanged = {
            // TODO REFACTOR get rid of bespoke functions for sorting and just pass Sorting to one general function
            when (it.sortingInfo.field) {
                FoodListSorting.Field.DEFAULT -> foodListViewModel.sortByDefault(it.sortingInfo.direction)
                FoodListSorting.Field.DATE -> foodListViewModel.sortByDate(it.sortingInfo.direction)
                FoodListSorting.Field.NAME -> foodListViewModel.sortByName(it.sortingInfo.direction)
            }
        },
        scaffoldState = scaffoldState,
        currentGrouping = currentGrouping,
        onGroupingChanged = {
            foodListViewModel.groupList(it)
        }
    )
}

@Composable
fun AddFoodActivityScreen(
    foodListViewModel: FoodListViewModel, navController: NavController
) {
    AddFoodScreen(
        onCloseScreen = {
            navController.navigateUp()
        },
        onConfirmFood = {
            foodListViewModel.addFood(it)
            navController.navigate(Screens.FoodList.route) {
                popUpTo(navController.graph.findStartDestination().id)
            }
        },
        categories = foodListViewModel.getAllCategories(),
        storageLocations = foodListViewModel.getAllStorageLocations()
    )
}

@Composable
fun FoodDetailActivityScreen(
    foodItem: FoodItem,
    foodListViewModel: FoodListViewModel,
    navController: NavController
) {
    FoodDetailScreen(foodItem = foodItem,
        onCloseScreen = {
            navController.navigateUp()
        },
        onDeleteFoodItem = {
            Log.i("FoodDetailActivityScreen", "User deleting food ${it.name}")
            navController.navigateUp()
            foodListViewModel.markFoodForRemoval(it)
        },
        onUpdateFoodItem = {
            foodListViewModel.updateFoodItem(it)
        }
    )
}
