package dev.flomhr.spoilist.ui

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = green800,
    primaryVariant = greenDark,
    onPrimary = white,

    secondary = green800,
    secondaryVariant = greenDark,
    onSecondary = white,

    background = black,
    onBackground = white,
    surface = black,
    onSurface = white,

    error = red700,
    onError = white
)

private val LightColorPalette = lightColors(
    primary = green800,
    primaryVariant = greenDark,
    onPrimary = white,

    secondary = orange500,
    secondaryVariant = orangeDark,
    onSecondary = black,

    background = white,
    onBackground = black,
    surface = white,
    onSurface = black,

    error = red900,
    onError = white
)

@Composable
fun SpoiListTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable() () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = typography,
        shapes = shapes,
        content = content
    )
}