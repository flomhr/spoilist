package dev.flomhr.spoilist.ui

import androidx.compose.ui.graphics.Color

val green800 = Color(0xFF2E7D32)
val greenLight = Color(0xFF60ac5d)
val greenDark = Color(0xFF004f04)
val orange500 = Color(0xFFFF9800)
val orangeLight = Color(0xFFffc947)
val orangeDark = Color(0xFFc66900)

val white = Color(0xFFFFFFFF)
val black = Color(0xFF000000)

val red900 = Color(0xFFB71C1C)
val red700 = Color(0xFFD32F2F)