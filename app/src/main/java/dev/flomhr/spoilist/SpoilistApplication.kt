package dev.flomhr.spoilist

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SpoilistApplication : Application() {

}