package dev.flomhr.spoilist

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import dev.flomhr.spoilist.food.persistence.FoodConverter
import dev.flomhr.spoilist.food.persistence.FoodItem
import dev.flomhr.spoilist.food.persistence.FoodItemDao

@Database(
    entities = [FoodItem::class],
    version = 2,
    exportSchema = true,
)
@TypeConverters(FoodConverter::class)
abstract class SpoilistDatabase : RoomDatabase() {

    abstract fun foodItemDao(): FoodItemDao

    companion object {

        fun createDatabase(context: Context): SpoilistDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                SpoilistDatabase::class.java,
                "spoilist_database"
            )
                .addMigrations(MIGRATION_1_2)
                .build()
        }
    }
}


val MIGRATION_1_2 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE food_items ADD COLUMN 'storage' TEXT NOT NULL DEFAULT ''")
    }

}