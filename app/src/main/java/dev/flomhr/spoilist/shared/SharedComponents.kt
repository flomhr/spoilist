package dev.flomhr.spoilist.shared

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import dev.flomhr.spoilist.R
import dev.flomhr.spoilist.ui.SpoiListTheme
import dev.flomhr.spoilist.ui.typography

@Composable
fun DialogWrapper(
    title: String,
    content: @Composable () -> Unit,
    buttons: @Composable () -> Unit = {},
    onDismissed: () -> Unit = {},
    properties: DialogProperties = DialogProperties()
) {
    Dialog(
        onDismissRequest = onDismissed,
        properties = properties,
    ) {
        DialogContent(
            title,
            content,
            buttons
        )
    }
}

@Composable
private fun DialogContent(
    title: String,
    content: @Composable () -> Unit,
    buttons: @Composable () -> Unit
) {
    Surface {
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.Start
        ) {
            Column(modifier = Modifier.padding(horizontal = 16.dp, vertical = 16.dp)) {
                Text(
                    text = title,
                    style = typography.h6,
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                content()
            }
            buttons()
        }
    }
}


@Composable
fun DialogButtons(
    modifier: Modifier = Modifier,
    onPositiveClicked: () -> Unit,
    positiveText: String,
    onNegativeClicked: () -> Unit,
    negativeText: String,
) {
    Row(
        modifier = modifier
            .padding(bottom = 8.dp)
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.End
    ) {
        TextButton(onClick = {
            onNegativeClicked()
        }) {
            Text(text = negativeText)
        }
        TextButton(
            onClick = {
                onPositiveClicked()
            },
        ) {
            Text(text = positiveText)
        }
    }
}


@Composable
fun <T> DropdownField(
    modifier: Modifier = Modifier,
    initialValue: T,
    valueOptions: List<T>,
    onValuePicked: (T) -> Unit,
    valueFormatting: (T) -> String = { it.toString() }
) {
    val (showDropdown, setShowDropdown) = remember { mutableStateOf(false) }

    Box(modifier = modifier) {
        Row(
            modifier = Modifier.clickable(onClick = { setShowDropdown(true) })
        ) {
            Text(
                modifier = Modifier.padding(horizontal = 4.dp),
                text = valueFormatting(initialValue),
                style = MaterialTheme.typography.body1,
            )
            Icon(
                painter = painterResource(id = if (showDropdown) R.drawable.ic_dropdown_up else R.drawable.ic_dropdown_down),
                tint = MaterialTheme.colors.onBackground,
                contentDescription = null
            )
        }
        val dropdownTestTag = stringResource(R.string.dropdownField_dropdownMenu)
        DropdownMenu(
            modifier = Modifier
                .heightIn(max = 256.dp)
                .testTag(dropdownTestTag),
            expanded = showDropdown,
            onDismissRequest = { setShowDropdown(false) }) {
            valueOptions.forEach {
                DropdownMenuItem(onClick = {
                    onValuePicked(it)
                    setShowDropdown(false)
                }) {
                    Text(text = valueFormatting(it))
                }
            }
        }
    }
}

@Preview(showBackground = true, group = "Component")
@Composable
fun PreviewDialogContent() {
    SpoiListTheme {
        DialogContent(
            title = "Test Dialog",
            content = {
                Row {
                    Text(text = "Some")
                    Image(
                        painter = painterResource(id = R.drawable.ic_launcher_foreground),
                        contentDescription = "Icon",
                        modifier = Modifier
                            .width(32.dp)
                            .padding(4.dp)
                    )
                    Text(text = "Content")
                }
            },
            buttons = {
                DialogButtons(
                    onPositiveClicked = { },
                    positiveText = "Yes",
                    onNegativeClicked = {},
                    negativeText = "No"
                )
            }
        )
    }
}

@Preview(showBackground = true, group = "Component")
@Composable
fun PreviewDialogButtons() {
    SpoiListTheme {
        DialogButtons(
            positiveText = "OK",
            onPositiveClicked = {},
            negativeText = "Cancel",
            onNegativeClicked = {},
        )
    }
}

@Preview(showBackground = true, group = "Component")
@Composable
fun PreviewDropdownField() {
    SpoiListTheme {
        DropdownField(
            initialValue = "Test",
            valueOptions = listOf("Test", "Asdf"),
            onValuePicked = {}
        )
    }
}