# SpoiList 🚯

SpoiList is an app to keep track your groceries 🛒 and when they will expire 📅 helping you reduce food waste 🗑 (and save money! 💰).

This is mostly meant as an exercise and showcase project, but feel free to try out the app, give feedback or share it with friends.

## Technologies used

- [Jetpack Compose](https://developer.android.com/jetpack/compose) for app UI
- [Room](https://developer.android.com/jetpack/androidx/releases/room) for persistence
- Gitlab CI for automated testing

## How to try the app

1. Clone this repository.
2. Open the project in [Android Studio](https://developer.android.com/studio).
3. Run the app on your phone.

I may start to do releases on here as well. If that happens this text will be updated!

#

Developed by Florian Mairhuber (*flomhr*), (c) 2021